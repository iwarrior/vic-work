<?php

$isProduction = true;
$version = "2-8-1";

// always diable production on localhost
if(in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1') )){
    $isProduction = false;
}



//$requirestUri = substr($_SERVER['REQUEST_URI'],1); // removes the lead "/"
$requirestUri = strtok($_SERVER["REQUEST_URI"],'?');
$requirestUri = substr($requirestUri,1); // removes the lead "/"

if (strpos($requirestUri,'?')) $requirestUri = substr($requirestUri,0,strpos($requirestUri,'?'));
if (substr($requirestUri,-1,1)=='/') $requirestUri = substr($requirestUri,0,-1);

print $requirestUri;

$isLoggedIn = false;
$cookieName = 'session';
if (@$_COOKIE[$cookieName] || @$_COOKIE['remember_token']) {
    $isLoggedIn = true;
}


// TRAFFIC
// referrer
if (isset($_SERVER['HTTP_REFERER']) && !isset($_COOKIE['tref'])) {
    setcookie("tref", $_SERVER['HTTP_REFERER'], time()+3600*24*30);
}
// source
if (isset($_REQUEST['tsrc']) && !isset($_COOKIE['tsrc'])) {
    setcookie("tsrc", $_REQUEST['tsrc'], time()+3600*24*30);
}

//function openLogin() {
//    require_once ('site/pages/auth/login/login.php');
//}


switch ($requirestUri) {

    case '':
    case '/':

        if ($isLoggedIn) require_once ('app/web/index.php');
        else require_once ('site/pages/home/home.php');
        break;

    case 'in-action':
        require_once ('site/pages/in-action/in-action.php');
        break;

    case 'help':
        header('Location: /help/');
        die();
        break;

//    case 'in-action/how-to-organize-everyone': require_once ('site/pages/in-action/how-to-organize-everyone.php'); break;
    case 'in-action/where-efforts-go': require_once ('site/pages/in-action/ia-1.php'); break;
    case 'in-action/how-to-organize-everyone': require_once ('site/pages/in-action/ia-2.php'); break;
    case 'in-action/bottlenecks': require_once ('site/pages/in-action/ia-3.php'); break;
    case 'in-action/teamboard': require_once ('site/pages/in-action/ia-4.php'); break;
    case 'in-action/performance': require_once ('site/pages/in-action/ia-5.php'); break;
    case 'in-action/clear-goals-priorities': require_once ('site/pages/in-action/ia-6.php'); break;
    case 'in-action/accountability': require_once ('site/pages/in-action/ia-7.php'); break;
    case 'in-action/overwhelmed-with-tasks': require_once ('site/pages/in-action/ia-8.php'); break;
    case 'in-action/focusing-on-success': require_once ('site/pages/in-action/ia-9.php'); break;
    case 'in-action/who-and-when': require_once ('site/pages/in-action/ia-10.php'); break;
    case 'in-action/collaboration-delays': require_once ('site/pages/in-action/ia-11.php'); break;


    case 'unsubscribe': require_once ('site/pages/email_marketing/unsubscribe.php'); break;

    case 'news': require_once ('site/pages/news/all.php'); break;
    case 'news/1016': require_once ('site/pages/news/1610.php'); break;
    case 'news/1116': require_once ('site/pages/news/1611.php'); break;
    case 'news/1216': require_once ('site/pages/news/1612.php'); break;
    case 'news/0117': require_once ('site/pages/news/1701.php'); break;
    case 'news/0217': require_once ('site/pages/news/1702.php'); break;
    case 'news/1017': require_once ('site/pages/news/1710.php'); break;
    case 'news/0318': require_once ('site/pages/news/1803.php'); break;
    case 'news/0418': require_once ('site/pages/news/1804.php'); break;
    case 'news/0518': require_once ('site/pages/news/1805.php'); break;
    case 'news/0618': require_once ('site/pages/news/1806.php'); break;
    case 'news/0718': require_once ('site/pages/news/1807.php'); break;
    case 'news/0818': require_once ('site/pages/news/1808.php'); break;
    case 'news/0918': require_once ('site/pages/news/1809.php'); break;
    case 'news/1018': require_once ('site/pages/news/1810.php'); break;
    case 'news/1118': require_once ('site/pages/news/1811.php'); break;
    case 'news/1218': require_once ('site/pages/news/1812.php'); break;


    case 'product': require_once ('site/pages/product/product.php'); break;
    case 'pricing': require_once ('site/pages/pricing/pricing.php'); break;
    case 'contact': require_once ('site/pages/other/contact.php'); break;
    case 'support': require_once ('site/pages/support/support.php'); break;
    case 'tos': require_once ('site/pages/other/tos.php'); break;
    case 'privacy': require_once ('site/pages/other/privacy.php'); break;

    case 'login':
                    if ($isLoggedIn) header('Location: /');
                    else {
                        // openLogin(); -> globals ...
                        require_once('site/pages/auth/login/login.php');
                    }
                    break;

    case 'login-google': require_once ('site/pages/auth/google/login.php'); break;
    case 'desktop': require_once ('site/pages/auth/force-desktop.php'); break;

    case 'join': require_once ('site/pages/auth/new-account/registration.php'); break;
    case 'join/email-confirmation': require_once ('site/pages/auth/new-account/email-sent.php'); break;
    case 'join/activation': require_once ('site/pages/auth/new-account/activation.php'); break;
    case 'join/welcome': require_once ('site/pages/auth/new-account/welcome.php'); break;

    case 'email-confirmation': require_once ('site/pages/auth/email-confirmation/email-confirmation.php'); break;
    case 'email-confirmation/welcome': require_once ('site/pages/auth/email-confirmation/welcome.php'); break;

    case 'password-recovery': require_once ('site/pages/auth/password/reset-form.php'); break;
    case 'password-recovery/sent': require_once ('site/pages/auth/password/email-sent.php'); break;
    case 'password/reset': require_once ('site/pages/auth/password/reset.php'); break;
    case 'password/reset-confirmation': require_once ('site/pages/auth/password/reset-confirmation.php'); break;

    case 'join/invitation': require_once ('site/pages/auth/invitation/invitation.php'); break;

    case 'slack': require_once ('site/pages/slack/slack.php'); break;

    case 'paypal_confirmation': require_once ('site/pages/other/paypal_payment_confirmation.php'); break;



    case '125': require_once ('app/web/recording_125.php'); break;
    case '110': require_once ('app/web/recording_110.php'); break;


    default:

        if ($isLoggedIn) require_once ('app/web/index.php');
        else require_once ('site/pages/auth/login/login.php'); //openLogin();

//
//
//        $fileName = 'site/pages/'.$requirestUri.'.php';
//        if (file_exists('site/pages/'.$requirestUri.'.php')) {
//            require_once ('site/pages/'.$requirestUri.'.php');
//        } else {
//            //ToDo: Better 404 ? kogda user logged out link starii... mozet na login kidat' ?
//            require_once ('site/pages/auth/login.php');
//        }
        break;
}


?>