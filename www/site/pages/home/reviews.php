<?php

$reviews = [

    4   => [
        'title'     => 'Makes managing multiple projects very easy and visual.',
        'name'      => 'Olga Zakharenkava',
        'company'   => 'Doxim',
        'message'   => ' This project management tool is very powerful, yet very easy to use, adoption is very quick, and the interface is very modern and intuitive. All features and customization capabilities any team would need in a project, task, and collaboration management tool without the high cost or lengthy implementation process.',
        'source'      => 'capterra'
    ],


    3   => [
        'title'     => 'Great workflow management tool.',
        'name'      => 'Janet J.',
        'company'   => 'Telmetrics',
        'message'   => 'The platform makes it easy to schedule and track the progress of any size of project from the multi-step, long-term and complex projects with multiple stakeholders to the small, one-step tasks.',
        'source'      => 'g2crowd'
    ],

    1   => [
        'title'     => 'Really easy to get started, but the software is so powerful!',
        'name'      => 'Joanne Fields',
        'company'   => 'Swapz',
        'message'   => 'It is very customizable and easy to use at the same time. All features of a great project management tool, but the interface is so well-designed, that if you do not need something - it does not clutter your workspace, unlike many other systems that try to look \'sophisticated\'.',
        'source'      => 'crowdreviews'
    ],


    2   => [
        'title'     => 'Works like a charm!',
        'name'      => 'Evangelos F.',
        'company'   => 'Pamediakopes',
        'message'   => 'All functionality of other great project management tools is there and it’s amazingly easy to use. Interface is top-notch, and an easy mobile version is just what I needed to stay on top of things if I’m away from the office. It also takes no time to train others on this. Those quick tutorials, when you just get started, are useful.',
        'source'      => 'g2crowd'
    ],

    5   => [
        'title'     => 'Our communication is better than ever.',
        'name'      => 'Andrew Z.',
        'company'   => 'G2 Crowd',
        'message'   => 'It does everything it sets out to do. Coming from a clunky and overly complicated project management tool previously - which will go unnamed - it\'s like night and day. Our team is more in sync and efficient, and our communication is better than ever.',
        'source'      => 'g2crowd'
    ],

    6   => [
        'title'     => 'Advanced, flexible project management platform.',
        'name'      => 'Alena Slayton',
        'company'   => 'Rockwell Collins',
        'message'   => 'GoodDay will be as customized and as flexible as you need to make it to reflect your real-life workflows. All configurations take just a few clicks, user training is very simple, and my team loves it. We manage all our goals, priorities, projects, and tasks there.',
        'source'      => 'capterra'
    ],



];


?>




<section class="reviews">
    <block class="reviews__content">
        <h2 class="reviews__h2">Reviews</h2>
        <div class="reviews__list">


            <?php

            foreach ($reviews as $review) {

                ?>
                <div class="reviews__item">
                    <div class="reviews__stars score-5"></div>
                    <div class="reviews__source"><img src="/site/assets/img/home/reviews/<?=$review['source']?>.png" /></div>
                    <div class="reviews__title">“<?=$review['title']?>”</div>
                    <div class="reviews__text"><?=$review['message']?></div>
                    <div class="reviews__name"><?=$review['name']?>, <?=$review['company']?></div>
                </div>

                <?php

            }


            ?>


        </div>
    </block>
</section>
<!-- ******** end reviews ******** -->