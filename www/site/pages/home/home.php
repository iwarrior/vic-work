<?php
require_once ('./site/pages/common/global_head.php');
require_once ('./site/pages/common/navigation.php');
?>

<script>

    $(function(){

        start.initForm("start-home-header");
        // tabs
        $('.visualize__item').on('click', function() {
        $(this)
          .addClass('active').siblings().removeClass('active')
          .closest('.visualize').find('div.visualize__tabContent').removeClass('active').eq($(this).index()).addClass('active');
      });
    });

</script>




<section class="header-main">

    <block style="overflow: hidden;">
        <div class="messaging">
            <h1 class="header-main__h1">Productive Work and Inspiring Management</h1>
            <p class="header-main__subtitle">based on Transparency, Motivation & Agility</p>
            <form name="start-home-header" action="">

                <div>
                    <input type="text" class="white" placeholder="Your @ email" name="email">
                    <a href="javascript:void(0)" class="submit">Get Started</a>
                </div>

                <div class="free">
                    25 Users, All features. Free.
                </div>

            </form>


        </div>

    </block>

</section>

<section class="clients">

    <block>
        <img src="/site/assets/img/home/clients.png" data-rjs="2">
    </block>

</section>

<section class="home wigd">

    <block>

        <div class="c-4-left">

            <h2>What is GoodDay</h2>

            <p class="content">
                GoodDay is a modern work management platform that brings together the best tools for high-level planning, project and product management, task organization and productivity growth based on transparency, agility, and motivation.
             </p>
            <!-- <div class="separator"></div> -->

            <ul class="icons">
                <li class="icons__item"><img data-rjs="2" src="/site/assets/img/home/eye.png"></li>
                <li class="icons__item"><img data-rjs="2" src="/site/assets/img/home/run.png"></li>
                <li class="icons__item"><img data-rjs="2" src="/site/assets/img/home/heart.png"></li>
                <li class="icons__item"><img data-rjs="2" src="/site/assets/img/home/company.png"></li>
            </ul>

            <p>
                <a class="video-btn" data-fancybox href="https://www.youtube.com/watch?v=CeGcJcar_z4">Watch product overview</a>
            </p>



        </div>
        <div class="c-6-right">
                <a class="wigd__play" data-fancybox href="https://www.youtube.com/watch?v=CeGcJcar_z4"></a>
                <img src="/site/assets/img/home/wigd-screen.png" height="10" data-rjs="2">

        </div>


    </block>


</section>

<!-- ******** begin numbers ******** -->
<section class="numbers">
    <block>
        <ul class="numbers__list">
            <li class="numbers__item">
                <img class="numbers__icon" data-rjs="2" src="/site/assets/img/home/org.png" alt="">
<!--                <span class="numbers__num">21k+</span>-->
                <span class="numbers__num">22,500<span class="numbers__plus">+</span></span>
                <span class="numbers__name">Organizations</span>
            </li>
            <li class="numbers__item">
                <img class="numbers__icon" data-rjs="2" src="/site/assets/img/home/user.png" alt="">
                <span class="numbers__num">248,000<span class="numbers__plus">+</span></span>
                <span class="numbers__name">Active users</span>
            </li>
            <div class="numbers__border"></div>
            <li class="numbers__item">
                <img class="numbers__icon" data-rjs="2" src="/site/assets/img/home/proj.png" alt="">
                <span class="numbers__num">370,000<span class="numbers__plus">+</span></span>
                <span class="numbers__name">Projects</span>
            </li>
            <li class="numbers__item">
                <img class="numbers__icon" data-rjs="2" src="/site/assets/img/home/task.png" alt="">
                <span class="numbers__num">15,600,000<span class="numbers__plus">+</span></span>
                <span class="numbers__name">Completed tasks</span>
            </li>
        </ul>
    </block>         
</section>
<!-- ******** end numbers ******** -->

<section class="gray home planning">

    <block>

        <div class="">

            <h2>Connect Goals, Strategy and Execution</h2>

            <p class="subtitle">
                GoodDay allows you to put together your business goals and strategic planning, organize and manage project execution, track progress towards KPIs, results and achievements. All within one platform.
            </p>

            <!-- <div class="tags">
                <div class="tag">Behaviour</div>
                <div class="tag">Communication and Processes</div>
                <div class="tag">Time & Efforts</div>
                <div class="tag">Tasks</div>
                <div class="tag">Behaviour</div>
            </div> -->
            <div class="steps">
                <div class="steps__item"><img class="steps__img" src="/site/assets/img/home/idea.png" alt=""></div>
                <div class="steps__item"><img class="steps__img" src="/site/assets/img/home/time.png" alt=""></div>
                <div class="steps__item"><img class="steps__img" src="/site/assets/img/home/taskp.png" alt=""></div>
                <div class="steps__item last"><img class="steps__img" src="/site/assets/img/home/done.png" alt=""></div>
            </div>
        </div>
        <div class="planning__imgWrap">
            <img class="planning__img" data-rjs="2" src="/site/assets/img/home/p1.jpg" alt="">
            <img class="planning__img" data-rjs="2" src="/site/assets/img/home/p2.jpg" alt="">
            <img class="planning__img" data-rjs="2" src="/site/assets/img/home/p3.jpg" alt="">
            <img class="planning__img" data-rjs="2" src="/site/assets/img/home/p4.jpg" alt="">
        </div>
        <!-- <div>
            <img src="/site/assets/img/home/plan-screen.png" style="max-width: 630px;" class="snapshot">
        </div>
        

        <div class="c-6-left mobile">

            <img src="/site/assets/img/home/plan-screen.png" style="max-width: 630px;" class="snapshot">

        </div> -->


    </block>


</section>


<!-- ******** begin visualize ******** -->
<section class="visualize">
    <block class="visualize__content">
        <img src="/site/assets/img/home/visualize.png" data-rjs="2" alt="">
        <h2 class="visualize__h2">Visualize Your Work</h2>
        <div class="visualize__subtitle subtitle">GoodDay gives managers and team members a quick, at-a-glance view into what's going on. At any time, see your overall workload, know what to work on next, understand current priorities, and be able to easily re-organize work when the business goals change.
        </div>
        <div class="visualize__tabs">
            <div class="visualize__item active">My Work</div>
            <div class="visualize__item">Calendar</div>
            <div class="visualize__item">What's Done</div>
            <div class="visualize__item">Kanban Board</div>
            <div class="visualize__item">Tasks Planning</div>
            <div class="visualize__item">Timeline</div>
            <div class="visualize__item">User Activity</div>
            <div class="visualize__item">Today</div>
            <div class="cf"></div>
        </div>
    </block>
    <div class="visualize__helper">
        <block>
            <div class="visualize__tabsContent">
                <div class="visualize__tabContent active">
                    <div class="visualize__imgWrap"><img data-rjs="2" class="visualize__img" src="/site/assets/img/home/visualize/v_1.png" alt=""></div>
                    <div class="visualize__info">
                        <div class="visualize__infoTitle">My Work</div>
                        <div class="visualize__infoText">GoodDay My Work is the ultimate dashboard for planning, scheduling, prioritizing, and executing on your tasks. At any time, get an at-a-glance view of your work all in one place, be clear on what to do next, get and stay organized.</div>
                        <a class="visualize__start submit" href="/join">Try now</a>
                    </div>
                </div>
                <div class="visualize__tabContent">
                    <div class="visualize__imgWrap"><img data-rjs="2" class="visualize__img" src="/site/assets/img/home/visualize/v_2.png" alt=""></div>
                    <div class="visualize__info">
                        <div class="visualize__infoTitle">Calendar</div>
                        <div class="visualize__infoText">With our powerful and flexible calendar, you can get a clear picture of what's planned across the entire company, department, or team, and, with just a few clicks, easily create and manage project, organization, and personal events and milestones.</div>
                        <a class="visualize__start submit" href="/join">Try now</a>
                    </div>
                </div>
                <div class="visualize__tabContent">
                    <div class="visualize__imgWrap"><img data-rjs="2" class="visualize__img" src="/site/assets/img/home/visualize/v_3.png" alt=""></div>
                    <div class="visualize__info">
                        <div class="visualize__infoTitle">What's Done</div>
                        <div class="visualize__infoText">
                            What's Done view is a perfectly organized summary of everything that has been accomplished, illustrating which milestones have been achieved and what tasks and projects have been completed, for any period of time.
                        </div>
                        <a class="visualize__start submit" href="/join">Try now</a>
                    </div>
                </div>
                <div class="visualize__tabContent">
                    <div class="visualize__imgWrap"><img data-rjs="2" class="visualize__img" src="/site/assets/img/home/visualize/v_4.png" alt=""></div>
                    <div class="visualize__info">
                        <div class="visualize__infoTitle">Kanban Board</div>
                        <div class="visualize__infoText">
                            With this always up-to-date Kanban board, all teams, onsite and remote, have a common understanding of the status of their work, a visual at-a-glance view of all workflows, and a tool for continuous process improvement.
                        </div>
                        <a class="visualize__start submit" href="/join">Try now</a>
                    </div>
                </div>
                <div class="visualize__tabContent">
                    <div class="visualize__imgWrap"><img data-rjs="2" class="visualize__img" src="/site/assets/img/home/visualize/v_5.png" alt=""></div>
                    <div class="visualize__info">
                        <div class="visualize__infoTitle">Tasks Planning</div>
                        <div class="visualize__infoText">
                            Our Tasks Planning view summarizes all work planned by users for each work day, helps analyze workload, provides data required to optimize work distribution and to boost team productivity, and enables transparency and accountability.
                        </div>
                        <a class="visualize__start submit" href="/join">Try now</a>
                    </div>
                </div>
                <div class="visualize__tabContent">
                    <div class="visualize__imgWrap"><img data-rjs="2" class="visualize__img" src="/site/assets/img/home/visualize/v_6.png" alt=""></div>
                    <div class="visualize__info">
                        <div class="visualize__infoTitle">Timeline</div>
                        <div class="visualize__infoText">
                            GoodDay Timeline is an extremely user-friendly and powerful Gantt Chart module that visualizes project schedules with all key dates and milestones and allows editing all critical project lifecycle elements on the fly.
                        </div>
                        <a class="visualize__start submit" href="/join">Try now</a>
                    </div>
                </div>
                <div class="visualize__tabContent">
                    <div class="visualize__imgWrap"><img data-rjs="2" class="visualize__img" src="/site/assets/img/home/visualize/v_7.png" alt=""></div>
                    <div class="visualize__info">
                        <div class="visualize__infoTitle">User Activity</div>
                        <div class="visualize__infoText">
                            To easily see your own or your team members' activity and accomplishments, use the User Activity view - our detailed live summary available for all users and providing insights for a particular day or any date range.
                        </div>
                        <a class="visualize__start submit" href="/join">Try now</a>
                    </div>
                </div>
                <div class="visualize__tabContent">
                    <div class="visualize__imgWrap"><img data-rjs="2" class="visualize__img" src="/site/assets/img/home/visualize/v_8.png" alt=""></div>
                    <div class="visualize__info">
                        <div class="visualize__infoTitle">Today Summary</div>
                        <div class="visualize__infoText">
                            To know what's happening and to stay organized, visualize every work day with Today Summary view, which shows all events, milestones, and tasks planned by all team members, department, or organization for any specific day.
                        </div>
                        <a class="visualize__start submit" href="/join">Try now</a>
                    </div>
                </div>
            </div>

        </block>
    </div>
</section>
<!-- ******** end visualize ******** -->

<!-- ******** begin bigScreen ******** -->
<section class="bigScreen">
    <block class="bigScreen__content">
        <div class="bigScreen__list">
            <div class="bigScreen__item">
                <img class="bigScreen__img" data-rjs="2" src="/site/assets/img/home/bigscr.jpg" alt="">
                <div class="bigScreen__iconWrap">
                    <img class="bigScreen__icon" data-rjs="2" src="/site/assets/img/home/bigscr_i.png" alt="">
                </div>
                <span class="bigScreen__title">Big Screens</span>
                <p class="bigScreen__p">Make the most out of GoodDay with Big Screens in every team area in your office. The important and actionable summaries on each screen will boost transparency, motivate positive change, break information silos, beat procrastination, and promote accountability.</p>
            </div>
            <div class="bigScreen__item">
                <img class="bigScreen__img" data-rjs="2" src="/site/assets/img/home/anal.jpg" alt="">
                <div class="bigScreen__iconWrap">
                    <img class="bigScreen__icon" data-rjs="2" src="/site/assets/img/home/anal_i.png" alt="">
                </div>
                <span class="bigScreen__title">Work. Analyze. Improve. </span>
                <p class="bigScreen__p">GoodDay collects and transforms all your work data into interactive reports and insightful infographics which help improve planning, collaboration, resource allocation and inspire personal productivity growth.</p>
            </div>
        </div>
    </block>
</section>
<!-- ******** end bigScreen ******** -->

<?php
require_once ('reviews.php');
?>

<!-- ******** begin about ******** -->
<section class="more">
    <block class="more__content">
        <div class="more__list">
            <div class="more__item">
                <img class="more__img" data-rjs="2" src="/site/assets/img/home/more_1.jpg" alt="">
                <div class="more__title">From Startup to Enterprise</div>
                <div class="more__text">
                    Designed to address the challenges of modern work organization, GoodDay helps small businesses grow and keeps large companies productive.
                </div>
            </div>
            <div class="more__item">
                <img class="more__img" data-rjs="2" src="/site/assets/img/home/more_2.jpg" alt="">
                <div class="more__title">Why GoodDay</div>
                <div class="more__text">
                    Recognition and connection to the bigger purpose keep people motivated at work. GoodDay automatically provides real-time feedback on achievements, productivity, organization skills, time management, and collaboration to keep teams inspired to do better work, every day.
                </div>
            </div>
            <div class="more__item">
                <div class="more__title">Foundation of a Good Work Day</div>
                <div class="more__text">
                    To make every work day really good, we combine great work and productivity tools with the key principles of modern management science and automate management best practices behind the scenes.
                </div>
                <img class="more__imgFrameWork" data-rjs="2" src="/site/assets/img/home/framework.png" alt="">
            </div>
            <div class="more__item more__item_buisness">
                <div class="more__title">Business Intelligence</div>
                <div class="more__text">
                    GoodDay Business Intelligence module empowers your team with real-time insights on all aspects of their work:
                </div>
                <img class="more__buisness" src="/site/assets/img/home/buisness.png" alt="">
                <ul class="more__conList">
                    <li class="more__conItem">Real reasons for work outcomes</li>
                    <li class="more__conItem">Progress against goals</li>
                    <li class="more__conItem">Bottlenecks</li>
                    <li class="more__conItem">Communication flows</li>
                    <li class="more__conItem">What happened and when</li>
                    <li class="more__conItem">And much more</li>
                </ul>
            </div>
            <div class="more__item moreAlways">
                <div class="moreAlways__title">Work <br>on the Go</div>
                <div class="moreAlways__text">Stay connected to your work and team no matter where you are with GoodDay mobile applications for iPhone, Android and your mobile browser.</div>
            </div>
            <div class="more__item moreInteg">
                <div class="moreInteg__title">Integrations</div>
                <div class="moreInteg__text">Connect GoodDay to the great <br> tools you are already using</div>
                <ul class="moreInteg__list">
                    <li class="moreInteg__item moreInteg__item_gmail"></li>
                    <li class="moreInteg__item moreInteg__item_dbox"></li>
                    <li class="moreInteg__item moreInteg__item_google"></li>
                    <li class="moreInteg__item moreInteg__item_slack"></li>
                    <li class="moreInteg__item moreInteg__item_calen"></li>
                </ul>
            </div>
        </div>
    </block>
</section>
<!-- ******** end about ******** -->

<script>
$('.reviews__list').slick({
    // arrows: false,
    // dots: true,
    // dotsClass: 'visualize__dots'
    slidesToShow: 3,
    prevArrow: '<button type="button" class="reviews__prev slick-prev"><img src="/site/assets/svg/rev_narrow.svg" width="13" height="21" alt="" /></button>',
    nextArrow: '<button type="button" class="reviews__next slick-next"></button>',
    responsive: [
    {
        breakpoint: 1100,
        settings:{
            slidesToShow: 2
        }
    },
    {
        breakpoint: 780,
        settings:{
            slidesToShow: 1
        }
    }
    ]
});
</script>
<?php
require_once ('./site/pages/common/footer.php');
?>