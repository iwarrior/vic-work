<?php
require_once ('./site/pages/common/blocks.php');

$version = $GLOBALS['version'];
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=8; IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>Good Day - Inspiring work management</title>

    <link rel="stylesheet" type="text/css" href="/site/assets/css/main.css?<?=$version?>">

    <script src="/site/assets/js/jquery-3.1.1.min.js?<?=$version?>">"></script>
    <script src="/site/assets/js/jquery.scrollto.js?<?=$version?>">"></script>
    <script src="/site/assets/js/jquery.nav.js?<?=$version?>">"></script>
    <script src="/site/assets/js/main.js?<?=$version?>"></script>
    <script src="/site/assets/js/moment.js?<?=$version?>">"></script>
    <script src="/site/assets/js/retina.min.js?<?=$version?>">"></script>
    <script src="/site/assets/js/slick.min.js?<?=$version?>">"></script>




    <link rel="apple-touch-icon" sizes="57x57" href="/common/favico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/common/favico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/common/favico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/common/favico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/common/favico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/common/favico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/common/favico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/common/favico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/common/favico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/common/favico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/common/favico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/common/favico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/common/favico/favicon-16x16.png">
    <link rel="manifest" href="/common/favico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/common/favico/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Global site tag (gtag.js) - Google Ads: 860762992 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-860762992"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'AW-860762992');
    </script>

    <!-- Google Login -->
    <meta name="google-signin-client_id" content="588823826282-42sefeoadp3sgafrp65qnokg19qlcg8t.apps.googleusercontent.com">
    <script src="https://apis.google.com/js/platform.js?onload=googleLogin_renderButton" async defer></script>




</head>
<body>

