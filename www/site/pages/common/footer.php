<footer>

    <block>

        <div class="c1">

            <img src="/site/assets/img/common/logo-white.png">

        </div>
        
        <div class="c2">
            <div class="col1">
                <a href="/">Home</a>
                <a href="/product">Product</a>
                <a href="/in-action">In action</a>
                <a href="/pricing">Pricing</a>
                <a href="/news">News & Updates</a>
            </div>
            <div class="col2">

                <a href="/support">Support</a>
                <a href="/contact">Contact us</a>
                <a href="/login">Customer login</a>
                <a href="/tos">Terms of service</a>
                <a href="/privacy">Privacy policy</a>
            </div>
            <div class="col3">
                <a href="https://www.facebook.com/gooddaywork/" target="_blank"><img src="/site/assets/img/common/f-facebook.png">Facebook</a>
                <a href="https://twitter.com/gooddaywork" target="_blank"><img src="/site/assets/img/common/f-twitter.png">Twitter</a>
                <a href="https://www.linkedin.com/company/goodday-work" target="_blank"><img src="/site/assets/img/common/f-linkedin.png">LinkedIn</a>
            </div>
        </div>
        
        
        <div class="c3">
            
            <div class="msg">
                25 Users. All features. Free
            </div>

            <form name="start-footer">

                <div>
                    <input type="text" class="white" placeholder="Your @ email" name="email">
                    <a href="javascript: void(0);" class="submit">Start</a>
                </div>

            </form>

            <div class="mobile-apps">

                <a href="https://itunes.apple.com/ca/app/goodday-work/id1343674351" target="_blank">
                    <img src="/site/assets/img/common/mobile-app-store.png" class="mob-ios">
                </a>

                <a href="https://play.google.com/store/apps/details?id=work.goodday.m.app" target="_blank">
                    <img src="/site/assets/img/common/mobile-google-play.png" class="mob-android">
                </a>


            </div>

            <div class="copyright">©2018 Createch Group, Inc.</div>
            
        </div>

        <div class="cf"></div>


    </block>


</footer>

<script>

    $(function(){

        start.initForm("start-footer");

    });

</script>

<?php
require_once('./site/pages/common/global_foot.php')
?>