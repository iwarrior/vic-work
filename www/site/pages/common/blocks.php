<?php


function pageHeader($title,$desc) {

    ?>

    <section class="header">
        <block>
            <div class="container">
                <h1><?=$title?></h1>
                <div class="l2"><?=$desc?></div>
            </div>
        </block>
    </section>
    <?php

}

function pageHeaderWithBack($title,$backTitle,$backLink) {


    ?>

    <section class="header back">
        <block>
            <div class="container">
                <div class="back-link">
                    <a href="<?=$backLink?>"><?=$backTitle?></a><span class="slash"> / </span>
                </div>
                <h1><?=$title?></h1>
            </div>
        </block>
    </section>
    <?php

}



?>