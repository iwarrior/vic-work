<?php
$isLoggedIn = false;
$cookieName = 'session';
if (@$_COOKIE[$cookieName] || @$_COOKIE['remember_token']) {
    $isLoggedIn = true;
}
//    $isLoggedIn = false;

?>

<nav>


    <block class="default">

        <a href="/"><img src="/site/assets/svg/logo-full.svg" class="logo"></a>

        <div class="menu">
            <a href="/product">Product</a>
            <a href="/in-action">In Action</a>
            <a href="/pricing">Pricing</a>
            <a href="/help/">Support</a>
        </div>


        <?php if (!$isLoggedIn) { ?>
            <div class="actions">
                <a href="/login" class="login">Login</a>
                <a href="/join" class="signup">Signup</a>
            </div>
        <?php } else { ?>

            <div class="actions">
                <a href="/" class="login-enter">My GoodDay</a>
            </div>

        <?php } ?>

    </block>

    <mobile>
        <div class="menu"><img src="/site/assets/img/common/menu.png" alt="menu"></div>
        <a href="/" class="gd-logo"></a>
        <a href="/login" class="login"><img src="/site/assets/img/common/login.png" alt="login"></a>
    </mobile>



</nav>


<div id="menu-mobile" class="hidden">

    <div class="top">
        <div class="logo"></div>
        <div class="close">
            <img src="/site/assets/img/common/close.png" alt="close">
        </div>

        <a href="/login" class="login"><img src="/site/assets/img/common/login.png" alt="login"></a>
    </div>

    <div class="main">

        <a href="/" class="link">Home</a>
        <div class="separator"></div>

        <a href="/product" class="link">Product</a>
        <div class="separator"></div>

        <a href="/in-action" class="link">In Action</a>
        <div class="separator"></div>

        <a href="/pricing" class="link">Pricing</a>
        <div class="separator"></div>

        <a href="/support" class="link">Support</a>
        <div class="separator"></div>

        <a href="/join" class="link">Register</a>
        <div class="separator"></div>

        <a href="/login" class="link">Login</a>
        <div class="separator"></div>

    </div>



</div>