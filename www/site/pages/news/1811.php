<?php

require_once ('./site/pages/news/common/head.php');


?>





<?php
pageHeaderWithBack("November '18","News","/news");
?>

    <section class="section news-block">
        <block>


            <h2>Updated MyWork</h2>


            <p class="desc">

                We’ve changed the interface of My Work, aligning it with the recently refreshed look of the project views.

                Now you can personalize the color of ‘My work’ and choose the widgets that you want to be displayed (Calendar, Notes, and Recent).

            </p>

            <img src="/site/assets/img/news/18-11/my-work.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>



    <section class="section news-block">
        <block>


            <h2>Task List / Task Table columns & presets</h2>


            <p class="desc">


                Now you can create your own columns presets in GoodDay! Save the columns you typically use in Table and
                List views and create your own presets that will be automatically applied when viewing task lists and tables.
                For example, choose to display Time and Progress columns and create a Time Reporting preset to focus your views on showing how much time is spent on tasks.
                You can also create and save a custom columns view for each individual project.


            </p>


            <img src="/site/assets/img/news/18-11/table-presets.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>

    <section class="section news-block">
        <block>


            <h2>New custom fields</h2>


            <p class="desc">
                We added the new custom field types that you can now add to any project and tasks, so that you could better manage your important data.

                Now your tasks can include fields of a dedicated format to save email addresses, links, GoodDay users, and ratings.
            </p>


            <img src="/site/assets/img/news/18-11/custom-fields.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>

<?php
require_once ('./site/pages/in-action/common/foot.php');
?>