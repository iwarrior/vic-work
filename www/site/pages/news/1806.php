<?php

require_once ('./site/pages/news/common/head.php');


?>









<?php

pageHeaderWithBack("June '18","News","/news");


?>

    <section class="section news-block">
        <block>


            <h2>All-new Tasks Board</h2>


            <p class="desc">
                Meet our all-new Board - we’ve released dramatic design and functional updates. Now here you can see all your tasks and projects on one screen without scrolling. Just change the tile sizes from small to large to make sure that you see all your tasks.
                Now you can also expand and collapse your task tiles to see details. With new filters and “fit to screen” button you can easily see all your tasks on one board no matter how big your screen is.

            </p>

            <img src="/site/assets/img/news/18-06/1.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>



    <section class="section news-block">
        <block>


            <h2>Enhanced Email Notifications</h2>


            <p class="desc">
                Check your updated Email Notifications. Now you can configure them by various parameters to receive only the most relevant updates. We have expanded the functionality so now you can configure the frequency and types of updates you want to receive.
            </p>


            <img src="/site/assets/img/news/18-06/2.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>



    <section class="section news-block">
        <block>


            <h2>Active Projects View</h2>


            <p class="desc">
                Our Active Projects View has changed. We’ve added projects tree and filters and made several design updates.
            </p>

            <img src="/site/assets/img/news/18-06/3.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>


    <section class="section news-block">
        <block>


            <h2>User Calendar</h2>

            <p class="desc">
                Check out the updated User Calendar - it is much easier to use now. We have added an informational panel which displays the number of working days, vacation time, sick leave days, and more.
            </p>

            <img src="/site/assets/img/news/18-06/4.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>


<?php
require_once ('./site/pages/in-action/common/foot.php');
?>