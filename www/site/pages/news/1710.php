<?php

require_once ('./site/pages/news/common/head.php');


?>









<?php

pageHeaderWithBack("October '17","News","/news");


?>






    <section class="section news-block">
        <block>


            <h2>All new notifications</h2>

            <p class="desc">

                We've completely revamped our Notifications to improve usability and draw attention to updates you can't afford to miss. Important Notifications (high priority and 'starred' items) and All Notifications are now shown separately, while advanced filters let you choose visible notifications manually.

            </p>

            <img src="/site/assets/img/news/17-oct/1.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>

    <section class="section news-block">
        <block>


            <h2>Starred</h2>


            <p class="desc">

                Now you can 'star' a project, task or user to mark items that are important to you. They will appear in the Starred section for quick navigation. 'Starred' enables many ways to bring attention to important items - e.g. on a Big Screen, in your one-on-one meeting, and more.

            </p>


            <img src="/site/assets/img/news/17-oct/2.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>

    <section class="section news-block">
        <block>


            <h2>Activity stream redesigned</h2>


            <p class="desc">
                We've improved the UI of activity stream and implemented better filtering options to help you stay on top of all updates you might be interested in. Now you can filter activities by date range, project, user, or activity type.
            </p>

            <img src="/site/assets/img/news/17-oct/3.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>



<?php
require_once ('./site/pages/in-action/common/foot.php');
?>