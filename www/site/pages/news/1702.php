<?php

require_once ('./site/pages/news/common/head.php');


?>









<?php

pageHeaderWithBack("February '17","News","/news");


?>






    <section class="section news-block">
        <block>


            <h2>Main Dashboard </h2>

            <p class="desc">

                The main dashboard was completely redesigned. We removed a few old, unnecessary widgets and made the overall look of
                the dashboard less busy yet more informative. The Summary widget now displays the self-organization chart and key
                statistics around your tasks and performance. The User widget provides insights on the user's tasks, schedule for the near future,
                and key behavioural metrics. The Project widget displays the main burndown chart, summary statistics, and distribution of work among the team members.

            </p>

            <img src="/site/assets/img/news/17-feb/1.png" class="snapshot web">
            <img src="/site/assets/img/news/17-feb/1-m.png" class="snapshot mobile">

        </block>
    </section>

    <section class="section news-block">
        <block>


            <h2>Project Tasks List </h2>


            <p class="desc">


                A number of significant improvements has been made to the task list. Now you can go inside the task group and
                navigate through your project structure with additional features to manage the group itself.
                To provide more horizontal space for additional columns, all filters and controls have been relocated from the right panel to the top.
                The top bar has been upgraded! All controls are now available in our new top bar, away from the main work area, to remove any distracting UI elements.
                For an even more user-friendly experience, all the tasks and groups can be reorganized with drag and drop and key details can be edited inline,
                without having to navigate away from the task list screen.

            </p>


            <img src="/site/assets/img/news/17-feb/2.png" class="snapshot uni">

        </block>
    </section>

    <section class="section news-block">
        <block>


            <h2>All projects</h2>


            <p class="desc">

                We have updated the main projects list module by enhancing the look and, more importantly, by adding many new features.
                All your projects are now organized into groups if you are a member of multiple organizations.
                All project data is editable inline which makes updating your projects' status and priorities quick, easy, and intuitive.
                We've also added a 'users' column for quick access and management of project teams.
            </p>

            <img src="/site/assets/img/news/17-feb/3.png" class="snapshot web">
            <img src="/site/assets/img/news/17-feb/3-m.png" class="snapshot mobile">

        </block>
    </section>



    <section class="section news-block last">
        <block>


            <h2>User management</h2>

            <p class="desc">

                We have completely redesigned the user management section to simplify the interface and make
                the experience more intuitive with the combined Organization Users and Pending Invitations modules.
                With the new organization role / department selector, now you can display users grouped not only by their access level but by department as well.
                You can also easily add new users directly from this main screen and see any invitations that are still pending.
            </p>

            <img src="/site/assets/img/news/17-feb/4.png" class="snapshot web">
            <img src="/site/assets/img/news/17-feb/4-m.png" class="snapshot mobile">

        </block>
    </section>

    <section class="section news-block">
        <block>


            <h2>Email templates redesigned</h2>

            <p class="desc">

                User notification email templates received a facelift.
                Now all key information is presented in a concise, visual format, using the projects’ color-coding for at-a-glance view into updates.

            </p>

            <img src="/site/assets/img/news/17-feb/5.png" class="snapshot uni" style="max-width: 768px;">

        </block>
    </section>


<?php
require_once ('./site/pages/in-action/common/foot.php');
?>