<?php

require_once ('./site/pages/news/common/head.php');


?>





<?php
pageHeaderWithBack("October '18","News","/news");
?>

    <section class="section news-block">
        <block>


            <h2>Updated Gantt Chart</h2>


            <p class="desc">

                We have released a completely redesigned Timeline which makes planning and executing on the complex projects easier.
                With improved auto-scheduling, you can now easily create dependencies between your tasks and folders. In addition,
                we added a full-screen mode, zoom and made a number of other usability updates.

            </p>

            <img src="/site/assets/img/news/18-10/gantt.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>



    <section class="section news-block">
        <block>


            <h2>Calendar: filters, settings, and improved design</h2>


            <p class="desc">


                GoodDay Calendar has been updated with design improvements and new features.
                Now you can filter your events not only by their types but also by user and by project, and you can choose to display or hide tasks deadlines.

                It is also much easier to manage events with the help of the new view that lets you display events for the week vs. a full month.

            </p>


            <img src="/site/assets/img/news/18-10/calendar.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>

    <section class="section news-block">
        <block>


            <h2>Help Center</h2>


            <p class="desc">
                The functionality of GoodDay is constantly expanding.
                To keep pace with all the new features, we have updated the Help Center, changed its structure, and created many
                new articles showing you how to work with GoodDay to achieve the best results for your team.

                The articles in our new Help Center are categorized according to the key functionality categories of GoodDay, so it’s easy to find the answers to all your questions.
            </p>


            <img src="/site/assets/img/news/18-10/help-article.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>

<?php
require_once ('./site/pages/in-action/common/foot.php');
?>