<?php

require_once ('./site/pages/news/common/head.php');


?>









<?php

pageHeaderWithBack("January '17","News","/news");


?>






    <section class="section news-block">
        <block>


            <h2>"New" section upgraded</h2>

            <p class="desc">

                The way you add a new task, project, user, or event has been significantly improved. Now, when creating a new task, you can immediately add it to a task group either from a list of recently used groups or from a project structure tree. The advanced parameters such as deadline, priority, estimate, etc. can be switched on/off in advanced options, so now you can adjust the interface to display only the fields you use on a regular basis. The minor functionality updates have been applied to new project and event sections along with this interface upgrade.
            </p>

            <img src="/site/assets/img/news/17-jan/1.png" class="snapshot web">
            <img src="/site/assets/img/news/17-jan/1-m.png" class="snapshot mobile">

<!--            <img src="/site/assets/img/news/17-jan/1-2.png" class="snapshot uni">-->

        </block>
    </section>

    <section class="section news-block">
        <block>


            <h2>My work v.4</h2>

            <p class="desc">
                My Work dashboard is one of the most frequently used modules and we continuously look for new ways to make it more functional and fun to use. In the newly updated interface, we kept the original calendar widget but added an infinite horizontal scroll, so now you can scroll to any day in the future without having to use the calendar navigation. Now you can also create new tasks right from My Work, automatically scheduling them to the desired date. Drag and drop into the calendar has been improved to make the drop action into the calendar faster and easier. Minor visual updates make for a more efficient use of space and a cleaner look.
            </p>

            <img src="/site/assets/img/news/17-jan/2.png" class="snapshot web">
            <img src="/site/assets/img/news/17-jan/2-m.png" class="snapshot mobile">


        </block>
    </section>

    <section class="section news-block">
        <block>


            <h2>Kanban boards enhanced</h2>

            <p class="desc">
                Kanban boards got a new, fresh look. All controls and filters have been moved to the top panel for better visual separation of content and controls. The columns and cards have been redesigned, similarly to My Work screen, any extra margins have been removed to use all available screen space more effectively. With this Boards view, you can categorize and view tasks not only by status but by action required user or priority. You can also create new tasks right from here.
            </p>

            <img src="/site/assets/img/news/17-jan/3.png" class="snapshot web">
            <img src="/site/assets/img/news/17-jan/3-m.png" class="snapshot mobile">

        </block>
    </section>



    <section class="section news-block last">
        <block>


            <h2>More</h2>

            <ul class="more" style="max-width: 450px;">
                <li><span class="gd-icon-add"></span>New loading screens and animations</li>
                <li><span class="gd-icon-add"></span>Project deletion now available under project settings</li>
                <li><span class="gd-icon-add"></span>Scrolling bar added for each column within task boards</li>
                <li><span class="gd-icon-add"></span>More colors available for Projects</li>
                <li><span class="gd-icon-add"></span>Other minor improvements and bug fixes. </li>
            </ul>

        </block>
    </section>




<?php
require_once ('./site/pages/in-action/common/foot.php');
?>