<?php

require_once ('./site/pages/news/common/head.php');


?>





<?php
pageHeaderWithBack("December '18","News","/news");
?>





    <section class="section news-block">
        <block>


            <h2>Project views management</h2>


            <p class="desc">


                We enhanced project settings and split the available settings into groups, including General Settings, Views, Workflow, and Integrations.

                In the General Settings section, you can choose tasks types to be available for the project as well as custom fields.
                Project views management options can be found in the Views section where you can define views that will be available for the project, as well as set the default view which will appear any time you first enter the project.

            </p>


            <img src="/site/assets/img/news/18-12/views-management.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>

    <section class="section news-block">
        <block>


            <h2>Custom task workflow</h2>


            <p class="desc">


                Workflow customization allows you to create a custom project workflow with a list of custom task statuses.
            </p>


            <img src="/site/assets/img/news/18-12/custom-workflow.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>


    <section class="section news-block">
        <block>


            <h2>Project activity view</h2>


            <p class="desc">

                The activity stream displaying all updates that relate to a particular project is now available via the More menu and allows to
                review the activity for that specific project only.


            </p>

            <img src="/site/assets/img/news/18-12/project-activity.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>


<?php
require_once ('./site/pages/in-action/common/foot.php');
?>