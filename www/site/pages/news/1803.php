<?php

require_once ('./site/pages/news/common/head.php');


?>









<?php

pageHeaderWithBack("March '18","News","/news");


?>






    <section class="section news-block">
        <block>


            <h2>All New Gantt Chart</h2>

            <p class="desc">

                We’ve improved our Gantt Chart by adding dynamic columns and adjustable panels. We’ve also made some design changes, improved UX, and made over 30 minor updates. Customize the chart according to your needs and see the big picture for all projects.

            </p>

            <img src="/site/assets/img/news/18-03/1.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>

    <section class="section news-block">
        <block>


            <h2>Desktop Notifications</h2>


            <p class="desc">

                Now you can enable desktop notifications for Windows and Mac OS and receive updates on tasks that require your attention even when GoodDay is not open.

            </p>


            <img src="/site/assets/img/news/18-03/2.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>

    <section class="section news-block">
        <block>


            <h2>Improved Slack Integration</h2>


            <p class="desc">
                Enhance your productivity by connecting Slack and GoodDay. You'll be able to get instant notifications and updates in Slack about what's happening on your projects and tasks, create and assign new tasks in GoodDay right from Slack.
            </p>

            <img src="/site/assets/img/news/18-03/3.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>



<?php
require_once ('./site/pages/in-action/common/foot.php');
?>