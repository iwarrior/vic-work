<?php

require_once ('./site/pages/news/common/head.php');


?>









<?php

pageHeaderWithBack("August '18","News","/news");


?>

    <section class="section news-block">
        <block>


            <h2>Project Templates</h2>


            <p class="desc">
                The biggest update of the summer is live.
                Now you can create your own project templates with custom project settings, subfolders, tasks, and events.
                Project template is a great time saver if you work with multiple projects that follow a similar workflow and/or include similar lists of task/events.

            </p>

            <img src="/site/assets/img/news/18-08/project-templates.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>



    <section class="section news-block">
        <block>


            <h2>Enhanced email integration</h2>


            <p class="desc">
                Email integration has been enhanced. Now you can select who can create new tasks via email and specify a default assignee user.
                Also, all email attachments will be automatically added to the task.

            </p>


            <img src="/site/assets/img/news/18-08/email-integration.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>





<?php
require_once ('./site/pages/in-action/common/foot.php');
?>