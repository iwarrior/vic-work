<?php

require_once ('./site/pages/news/common/head.php');



function renderInActionListItem($data) {



    ?>

    <div class="gd-in-action-item">
        <div class="l-block">
            <div class="image">
                <figure style="background-image: url('../assets/images/in-action/list/<?=$data['image']?>')"></figure>
            </div>
        </div>
        <div class="r-block">
            <div class="description">
                <h2><?=$data['title']?></h2>
                <div class="tegs-item">

                    <?php

                    foreach ($data['tags'] as $tag) {

                        ?>

                        <span class="st-color-<?=$tag['color']?>"><?=$tag['name']?></span>

                        <?php

                    }

                    ?>

                </div>
                <div class="descr">
                    <p><?=$data['description']?></p>
                </div>

                <div class="all-article"><a class="gd-button-solid" href="/in-action/<?=$data['url']?>">Read more</a></div>
            </div>
        </div>
    </div>


    <?php


}

function tag2class($name) {
    $class = str_replace(' ','-',$name);
    $class = strtolower($class);
    return $class;
}

function renderBlock($data,$separator=true) {



    ?>

    <section class="news-list n-<?=$data['id']?>">

        <block <?php if ($data['more']) { ?>onClick="document.location.href='/news/<?=$data['id']?>'" <?php } ?>>

            <div class="left">
                <img src="/site/assets/img/news/<?=$data['image']?>" alt="<?=$data['title']?>" class="snapshot"/>
            </div>

            <div class="right">
                <h2><?=$data['title']?></h2>



                <p><?=$data['description']?></p>

                <?php

                if (@$data['list']) {

                    ?><ul><?php

                    foreach ($data['list'] as $li) {
                        ?>
                        <li><?=$li?></li>
                        <?php
                    }

                    ?></ul><?php

                }



                ?>



                <?php

                $className = 'read-more';
                if (@$data['list']) $className.=' list';

                if ($data['more']) {
                    ?>
                    <a class="<?=$className?>" href="/news/<?=$data['id']?>">Read more</a>
                    <?php
                }
                ?>

            </div>

        </block>

    </section>




    <?php

}





pageHeader("NEWS & UPDATES","Major interface updates and new feature releases");


?>




<?php
foreach ($newsList as $key=>$item) {

    $separator = !($key==(sizeof($newsList)+1));

    renderBlock($item,$separator);

}
?>





<?php
require_once ('./site/pages/common/footer.php');
?>