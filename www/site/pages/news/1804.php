<?php

require_once ('./site/pages/news/common/head.php');


?>









<?php

pageHeaderWithBack("April '18","News","/news");


?>






    <section class="section news-block">
        <block>
            <h2>New My Work</h2>

            <p class="desc">
                We released the updated My Work with enhanced UI. The updated My Work screen now allows you to switch between “Action Required”, “Assigned to me” and “All involved in” views, with additional options for displaying your work items in list and board layouts.
            </p>

            <img src="/site/assets/img/news/18-04/1.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>

    <section class="section news-block">
        <block>
            <h2>Enhanced Project Navigation</h2>

            <p class="desc">
                We’ve improved project navigation by adding a panel that simplifies views and management access. Now, in the tab “More” you can find settings, edit your profile, update statuses and much more.
            </p>

            <img src="/site/assets/img/news/18-04/2.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>

    <section class="section news-block">
        <block>


            <h2>Past Due View</h2>

            <p class="desc">
                With our new past due view you will not miss any tasks that should be taken care of to meet set timelines. Check all projects, events and tasks that are past due in one place. Review start and end dates, deadlines and task owners. Be in the loop and on track with all your projects!
            </p>

            <img src="/site/assets/img/news/18-04/3.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>

    <section class="section news-block">
        <block>


            <h2>Multiple Email Addresses Support</h2>


            <p class="desc">
                Now you can add additional email addresses to your GoodDay account and choose the primary one. Adjust your notifications and get all updates to your valid email.
            </p>

            <img src="/site/assets/img/news/18-04/4.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>



<?php
require_once ('./site/pages/in-action/common/foot.php');
?>