<?php

require_once ('./site/pages/news/common/head.php');


?>









<?php

pageHeaderWithBack("November '16","News","/news");


?>






    <section class="section news-block">
        <block>


            <h2>Rich Text Formatting</h2>

            <p class="desc">

                We added an easy-to-use WYSIWYG text editor to give you the formatting tools, including the font styles, headings, lists, code snippets and more. The rich text editor can be toggled on and off by clicking T+ and is enabled for most text fields within the application.
            </p>

            <img src="/site/assets/img/news/16-nov/1.png" class="snapshot uni">

        </block>
    </section>

    <section class="section news-block">
        <block>


            <h2>Move to</h2>

            <p class="desc">

                For an easy way to keep projects organized, we added an option to move a task into any project, group, or task. Simply click Actions/Move to and choose the project from the list on the left to view its structure and choose the move destination.
            </p>


            <img src="/site/assets/img/news/16-nov/2.png" class="snapshot uni">


        </block>
    </section>

    <section class="section news-block">
        <block>


            <h2>Main menu reorganized</h2>

            <p class="desc">

                We reorganized and simplified the main menu by grouping related items together. Each expanding section (projects, summary, tasks, events, people) is now color-coded to visually emphasize the items within it. The “+” button has become larger for easier access to new task, user, organization, or event creation window, and the search bar is now always visible.

            </p>



            <img src="/site/assets/img/news/16-nov/3.png" class="snapshot uni" style="max-width: 768px;">

        </block>
    </section>


    <section class="section news-block">
        <block>


            <h2>Cleaner interface</h2>

            <p class="desc">

                To create more space for your main work area, we removed the top panel from the screens where it did not add value to the user experience. This meant a complete redesign of such screens as My Work, Events, Tasks, Boards, People, Reports and Analytics. We kept the top panel with the functional controls within the sections that require an additional easy-to-access navigation level.

            </p>

            <img src="/site/assets/img/news/16-nov/4.png" class="snapshot web">
            <img src="/site/assets/img/news/16-nov/4-m.png" class="snapshot mobile">

        </block>
    </section>


    <section class="section news-block last">
        <block>


            <h2>More</h2>

            <ul class="more" style="max-width: 380px;">
                <li><span class="gd-icon-add"></span>Adding file attachments in the task edit mode</li>
                <li><span class="gd-icon-add"></span>Attached files now can be open in a browser</li>
                <li><span class="gd-icon-add"></span>Other minor fixes and improvements</li>
            </ul>








        </block>
    </section>




<?php
require_once ('./site/pages/in-action/common/foot.php');
?>