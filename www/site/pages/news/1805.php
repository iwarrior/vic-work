<?php

require_once ('./site/pages/news/common/head.php');


?>









<?php

pageHeaderWithBack("May '18","News","/news");


?>

    <section class="section news-block">
        <block>


            <h2>Tasks List - Manual Order</h2>


            <p class="desc">
                Now you can manually reorder all your tasks with simple drag-and-drop. We have also improved the controls that allow you to filter your tasks by numerous parameters.
            </p>

            <img src="/site/assets/img/news/18-05/2.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>



    <section class="section news-block">
        <block>


            <h2>Mobile Notifications</h2>


            <p class="desc">
                Now you can enable mobile push notifications to receive updates directly on your phone.
            </p>


            <img src="/site/assets/img/news/18-05/1.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>



    <section class="section news-block">
        <block>


            <h2>Enhanced Organization User Settings</h2>


            <p class="desc">
                We’ve enhanced company User Settings. You can manage time reporting settings, work schedule, access to My Work and major organizational roles.
            </p>

            <img src="/site/assets/img/news/18-05/3.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>


    <section class="section news-block">
        <block>


            <h2>Task View </h2>

            <p class="desc">

                We've improved the reply form and added "Show More" option to tasks with long history of conversations. So now you don’t need to scroll down the page to find the latest comment in a task.

            </p>

            <img src="/site/assets/img/news/18-05/4.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>


<?php
require_once ('./site/pages/in-action/common/foot.php');
?>