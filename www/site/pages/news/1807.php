<?php

require_once ('./site/pages/news/common/head.php');


?>









<?php

pageHeaderWithBack("July '18","News","/news");


?>

    <section class="section news-block">
        <block>


            <h2>Task View</h2>


            <p class="desc">
                The Task View screen has been completely redesigned. While keeping the original layout, we implemented over 30 improvements including thr ToDo list, dependencies view,
                improved custom fields view, messages filter and more. Another handy enhancement is the ability to switch between modal, full and split view modes.
            </p>

            <img src="/site/assets/img/news/18-07/1.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>



    <section class="section news-block">
        <block>


            <h2>Analytics - Time Reports</h2>


            <p class="desc">
                We have split the original reporting module into 2 - report by project and report by user. Within each report, you can group data by project, date or user, include/exclude guesstimates and compare
                reported time to original estimates.
            </p>


            <img src="/site/assets/img/news/18-07/2.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>



    <section class="section news-block">
        <block>


            <h2>Efforts Distribution</h2>


            <p class="desc">
                The new Efforts Distribution analyzer helps you see the big picture of how your efforts were allocated by allowing you to "zoom-in" into any specific project
                for a detailed report.
            </p>

            <img src="/site/assets/img/news/18-07/3.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>


    <section class="section news-block">
        <block>


            <h2>Add User Modal</h2>

            <p class="desc">

                The new Add User modal screen allows adding multiple users at once to your tasks and projects. It also comes with Search by Name functionality which makes it easy
                to find a person within large lists of organization users.

            </p>

            <img src="/site/assets/img/news/18-07/4.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>


<?php
require_once ('./site/pages/in-action/common/foot.php');
?>