<?php

require_once ('./site/pages/news/common/head.php');


?>









<?php

pageHeaderWithBack("December '16","News","/news");


?>






    <section class="section news-block">
        <block>


            <h2>Kanban boards</h2>

            <p class="desc">
                Kanban board is a great tool to visualize and organize tasks.
                While working on this module, we went beyond the standard functionality and implemented the ability to not only group tasks by status but also by user and priority, which makes it super easy to prioritize and delegate work.
            </p>


            <img src="/site/assets/img/news/16-dec/1.png" class="snapshot uni">

        </block>
    </section>

    <section class="section news-block">
        <block>


            <h2>Project dashboard </h2>

            <p class="desc">
                The new project dashboard interface has become much more intuitive and better organized. It helps monitoring project activity, work delegation, team member workload and allows updating the project status and priority. Use your dashboard as the project home page - view all milestones and events in one place and quickly navigate to specific tasks.
            </p>

            <img src="/site/assets/img/news/16-dec/2.png" class="snapshot web">
            <img src="/site/assets/img/news/16-dec/2-m.png" class="snapshot mobile">


        </block>
    </section>

    <section class="section news-block">
        <block>


            <h2>Tasks groups</h2>

            <p class="desc">
                To give you one more advanced tool for getting organized, we implemented groups which don’t just help to structure the tasks but can also be used as subprojects with their own status, priority, deadline, estimates etc. The group view lets you manage, create, and organize tasks and provides group management controls. The progress chart will keep you updated on the overall progress of your subproject.

            </p>

            <img src="/site/assets/img/news/16-dec/3.png" class="snapshot uni">

        </block>
    </section>


    <section class="section news-block">
        <block>


            <h2>Subtasks</h2>

            <p class="desc">
                To maximize the flexibility of your projects’ structure, we added subtasks, which have the same functionality as the regular tasks but are always linked to a parent task. The subtasks navigation tree appears in the right panel of the task view screen along with all the management options.
            </p>

            <img src="/site/assets/img/news/16-dec/4.png" class="snapshot uni">

        </block>
    </section>


    <section class="section news-block last">
        <block>


            <h2>More</h2>

            <ul class="more">
                <li><span class="gd-icon-add"></span>Project Tasks List counters for the tasks that contain subtasks added.</li>
                <li><span class="gd-icon-add"></span>Inline task editing within project tasks list.</li>
                <li><span class="gd-icon-add"></span>Improved search</li>
                <li><span class="gd-icon-add"></span>Minor bugfixing and performance improvements</li>
            </ul>


        </block>
    </section>




<?php
require_once ('./site/pages/in-action/common/foot.php');
?>