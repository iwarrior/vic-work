<?php
//require_once ('./site/pages/in-action/common/list-config.php');
require_once ('./site/pages/common/global_head.php');
require_once ('./site/pages/common/navigation.php');

//$data = @$inActionList[$inActionId];
//
//
//
////topNavigation();

$newsList = [

    [
        'id' => '1218',
        'title' => "December '18",
        'description' => false,
        'image' => '18-12/dec-18.png',
        'version' => '3.08',
        'more'  => true,
        'list'  => [
            'Project views management now enables you to select a custom set of views for your folders and projects',
            'Custom task workflow to allow defining a list of custom statuses for each folder or project',
            'Project activity view',
        ]
    ],

    [
        'id' => '1118',
        'title' => "November '18",
        'description' => false,
        'image' => '18-11/nov-18.png',
        'version' => '3.08',
        'more'  => true,
        'list'  => [
            'Updated My Work with improved navigation and design',
            'List/Table columns management and view presets',
            'New custom fields including rating, email, phone, url, and user'
        ]
    ],

    [
        'id' => '1018',
        'title' => "October '18",
        'description' => false,
        'image' => '18-10/oct-18.png',
        'version' => '3.08',
        'more'  => true,
        'list'  => [
            'Major Gantt chart upgrade',
            'Calendar filters, settings, and improved design',
            'New and expanded Help Center'
        ]
    ],

    [
        'id' => '0918',
        'title' => "September '18",
        'description' => false,
        'image' => '18-09/sep-18.png',
        'version' => '3.08',
        'more'  => true,
        'list'  => [
            'All-new Task List view with advanced group by and sorting options, updated filters and fields management',
            'New Files section with support for project files and task attachment management',
        ]
    ],

    [
        'id' => '0818',
        'title' => "August '18",
        'description' => false,
        'image' => '18-08/aug-18.png',
        'version' => '3.08',
        'more'  => true,
        'list'  => [
            'Custom Project templates with support for tasks, subfolders, events, and project settings',
            'Enhanced email integration',
            'Improved loading speed'
        ]
    ],





    [
        'id' => '0718',
        'title' => "July '18",
        'description' => false,
        'image' => '18-07/preview.png',
        'version' => '3.08',
        'more'  => true,
        'list'  => [
            'Our most significant Task View screen update since version one, icluding 3 new layouts: modal, full, and split',
            'New Time Reports',
            'Upgraded Efforts Distribution analytics',
            'Enhanced Add/Select User modals'
        ]
    ],

    [
        'id' => '0618',
        'title' => "June '18",
        'description' => false,
        'image' => '18-06/preview.png',
        'version' => '3.08',
        'more'  => true,
        'list'  => [
            'New Tasks Board: fresh design, tile sizes, subfolders, group by, folding, and more',
            'Improved Email Notifications',
            'Active Project view',
            'Updated User Calendar',
            'Organization statuses management',
        ]
    ],

    [
        'id' => '0518',
        'title' => "May '18",
        'description' => false,
        'image' => '18-05/preview.png',
        'version' => '3.08',
        'more'  => true,
        'list'  => [
            'Manual ordering for Task List',
            'Mobile notifications',
            'Enhanced organization User Settings',
            'The no-scroll Task View refresh'
        ]
    ],

    [
        'id' => '0418',
        'title' => "April '18",
        'description' => false,
        'image' => '18-04/preview.png',
        'version' => '3.08',
        'more'  => true,
        'list'  => [
            'My Work: Action required, Assigned to me, All involved in views ',
            'Enhanced Folder/Project navigation',
            'Pastdue view',
            'Support for multiple email accounts'
        ]
    ],

    [
        'id' => '0318',
        'title' => "March '18",
        'description' => false,
        'image' => '18-03/preview.png',
        'version' => '3.08',
        'more'  => true,
        'list'  => [
            'All-new Gantt Chart: improved UX, dynamic columns, adjustable panels and more',
            'Desktop notifications',
            'Improved Slack integration',
            'Calendar filters',
            'Resource Planning view'

        ]
    ],



    [
        'id' => '0218',
        'title' => "February '18",
        'description' => false,
        'image' => '18-02/preview.png',
        'version' => '3.08',
        'more'  => false,
        'list'  => [
            'Major updates to the time tracking module',
            'Enhanced My Time view and time reporting from the task view',
            'Improved reports and CSV data export',
            'All-new personal reports with even more data now split into tabs',
            'Improved search, including search by task ID',
            'New GoodDay.work website design'
        ]
    ],

    [
        'id' => '0117',
        'title' => "January '18",
        'description' => '<b>All-new Android and iPhone apps:</b>',
        'image' => '18-01/preview.png',
        'version' => '3.08',
        'more'  => false,
        'list'  => [
            'Create and assign new tasks',
            'Browse your projects portfolio and     tasks',
            'Schedule, prioritize, and update statuses',
            'Advanced Search',
            'Events calendar',
            'Available for all types of GoodDay accounts'
        ]
    ],


    [
        'id' => '1217',
        'title' => "December '17",
        'description' => false,
        'image' => '17-12/preview.png',
        'version' => '3.08',
        'more'  => false,
        'list'  => [
            'Updated "Tasks by user" view with ability to switch between assigned to and action required options',
            '"What\'s done" view filtering by project/user',
            'Project calendar task deadlines',
            'Pinned items (projects/tasks/users) now appear in the main menu'
        ]
    ],

    [
        'id' => '1117',
        'title' => "November '17",
        'description' => false,
        'image' => '17-11/preview.png',
        'version' => '3.08',
        'more'  => false,
        'list'  => [
            'Recurring tasks with advanced options for user-defined scheduling',
            'Copy your tasks and projects and create project templates',
            'Project/Task converting options',
            'Activity Stream now displays not only task, but also project and event updates'
        ]
    ],


    [
        'id' => '1017',
        'title' => "October '17",
        'description' => 'This month, we have released several user experience improvements that enhance the way users get updated on activity in GoodDay. This includes redesigned notifications engine, a revamped activity stream, and the ability to \'star\' an important item.',
        'image' => '17-10/preview.png',
        'version' => '3.08',
        'more'  => true
    ],
    [
        'id' => '0917',
        'title' => "September '17",
        'description' => false,
        'image' => '17-09/preview.png',
        'version' => '3.08',
        'more'  => false,
        'list'  => [
                'New integrations, including Google Drive, Dropbox, and Box',
                'Improved My Work dashboard with additional filters and views',
                'Advanced task planning capabilities added',
                'Updated Event creation form with better usability and UI',
                'Improved Project Calendar'
        ]
    ],
    [
        'id' => '0817',
        'title' => "August '17",
        'description' => false,
        'image' => '17-08/preview.png',
        'version' => '3.08',
        'more'  => false,
        'list'  => [
            'New Project Events section with a calendar, events list view, and additional filters',
            'Improved filters for Big Screens and Views',
            'Updated email notifications engine',
            'Ability to convert Projects and Workfolders',
            'Mobile version upgrade',
            'New release of My Time section with updated UI, filters, and time reporting functionality',
            'Enhanced date selectors throughout GoodDay sections and modules'
        ]
    ],
    [
        'id' => '0717',
        'title' => "July '17",
        'description' => false,
        'image' => '17-07/preview.png',
        'version' => '3.08',
        'more'  => false,
        'list'  => [
            'All new tree-like project structure with unlimited nesting',
            'Enhanced hierarchy capabilities where projects and workfolders have unique functionality and provide unlimited flexibility in organizing work',
            'Updated project colors for better visualization',
            'Agile sprint and backlog support',
            'Enhanced project access control management'
        ]
    ],
    [
        'id' => '0617',
        'title' => "June '17",
        'description' => false,
        'image' => '17-06/preview.png',
        'version' => '3.08',
        'more'  => false,
        'list' => [
            'Custom task fields are now available and include text, number, checkbox, date, currency, percentage and more',
            'What\'s Done view released',
            'New Progress Summary View',
            'Updated Big Screens with additional filters and settings',
            'Gantt Chart usability and UI improvements'
        ]
    ],
    [
        'id' => '0517',
        'title' => "May '17",
        'description' => '',
        'image' => '17-05/preview.png',
        'version' => '3.08',
        'more'  => false,
        'list' => [
            'Enhanced our integrations with Google products, including Google Calendar, Gmail, and automatic login from Google account',
            'New Collaboration Analytics report for insight into the quality of collaboration between users and departments',
            'Additional Big Screens modules released, including Today\'s tasks and Project summary',
            'Enhanced User Dashboard screen to streamline widgets and activity streams',
            'UI and navigation improvements'
        ],

    ],
    [
        'id' => '0417',
        'title' => "April '17",
        'description' => false,
        'list' => [
                'Updated mobile version for access to GoodDay on the go',
                'New Boards screen for easy access to all your work summary Views',
                'Enhanced task list view with ability to add extra fields through columns modal menu',
                'Updated Events section with a new All Events list',
                'Bulk edit functionality to quickly move, set priority, action required, or schedule multiple items at a time',
                'New \'Import CSV\' module to quickly create multiple projects and tasks'

        ],
        'image' => '17-04/preview.png',
        'version' => '3.08',
        'more'  => false
    ],
    [
        'id' => '0317',
        'title' => "March '17",
        'description' => '',
        'list' => [
            'A feature-rich, easy to use Gantt Charts module for complete flexibility to view, create, plan, schedule tasks, define dependencies, and work with any time frame with the quick zoom option',
            'Upgraded Organization Dashboard with an improved layout and more space for key information',
            'Enhanced People section navigation and multi-organization support',
            'Updated main menu with a minimized view and ability to collapse all menu items',
            'Reworked tasks list now incudes a full tree view and inline editing',
            'Additional UI improvements and bug fixes'
        ],
        'image' => '17-03/preview.png',
        'version' => '3.08',
        'more'  => false
    ],

    [
        'id' => '0217',
        'title' => "February '17",
        'description' => 'This month, we are introducing the new, improved main dashboard design, the simplified project task list interface, the streamlined and reorganized user management sections, and the upgraded look of email notifications.',
        'image' => '17-feb/preview.png',
        'version' => '3.08',
        'more'  => true
    ],
    [
        'id' => '0117',
        'title' => "January '17",
        'description' => 'We made important changes to some of the most frequently used screens and modules. My work, kanban boards, and the add new screen now have a better look, a more intuitive user experience, and streamlined controls and navigation.',
        'image' => '17-jan/preview.png',
        'version' => '3.11',
        'more'  => true
    ],
    [
        'id' => '1216',
        'title' => "December '16",
        'description' => 'The recent updates focus on giving you an even better way to organize work and include the new project progress visualization section, boards, an updated project dashboard look, multi-level project hierarchy supporting subprojects and subtasks, and more.',
        'image' => '16-dec/preview.png',
        'version' => '3.11',
        'more'  => true
    ],
    [
        'id' => '1116',
        'title' => "November '16",
        'description' => 'Two major updates this month include the interface revamp - a reorganized main menu, an updated top navigation panel, and a new task hierarchy tree, and the addition of the Rich Text Editor to all main text editing fields within the application.',
        'image' => '16-nov/preview.png',
        'version' => '3.11',
        'more'  => true
    ],
    [
        'id' => '1016',
        'title' => "October '16",
        'description' => 'To make the user experience within the key areas of the application even better, we completely redesigned the project screen look, task list interface, task view and added consistent color-coding to the project and its tasks.',
        'image' => '16-oct/preview.png',
        'version' => '3.11',
        'more'  => true
    ],









];

?>




<!--<div id="main">-->

