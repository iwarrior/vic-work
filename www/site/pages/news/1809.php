<?php

require_once ('./site/pages/news/common/head.php');


?>





<?php
pageHeaderWithBack("September '18","News","/news");
?>

    <section class="section news-block">
        <block>


            <h2>Task List</h2>


            <p class="desc">

                We’ve created a completely new module, ‘Task List’, with an interface that is simpler compared to the Task Table view.

                The Task list allows to quickly focus on the big picture of the projects's and tasks' progress by grouping
                tasks by different criteria such as status, action required, assigned to and priority.

                <br />
                The wide range of customizable columns will show you all tasks details.

            </p>

            <img src="/site/assets/img/news/18-09/task-list.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>



    <section class="section news-block">
        <block>


            <h2>Project Files</h2>


            <p class="desc">


                The all-new Files section allows to add and organize all your project-related files.

                It also provides great functionality for browsing project task attachments which is handy when you need to find
                a specific file.

                The Files section comes with two views - list and thumbnails, as well as search and sort by options, and advanced filtering.

            </p>


            <img src="/site/assets/img/news/18-09/project-files.png" class="snapshot uni" style="max-width: 800px;">

        </block>
    </section>



<?php
require_once ('./site/pages/in-action/common/foot.php');
?>