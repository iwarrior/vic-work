<?php
require_once ('./site/pages/common/global_head.php');
require_once ('./site/pages/common/navigation.php');
?>

    <script>

        $(function(){

            start.initForm();

        });

    </script>



    <section class="header">
        <block>
            <div class="container">
                <h1>PLANS AND PRICING</h1>
                <div class="l2">Flexible plans to get started and grow</div>
            </div>
        </block>
    </section>

    <section class="pricing mounth">

        <block>
            <div class="time">
                <a class="time__link active" href="#mounth">Monthly</a>
                <a class="time__link" href="#year">Annually</a>
            </div>

            <div class="c1">
                <div class="helper">
                    <div class="plan">
                        <h2><img class="price__icon" src="/site/assets/img/pricing/free.png" alt="">Free</h2>
                        <p class="users">1-25 Users</p>
                        <p class="desc"><span class="desc__helper">Get all GoodDay modules, features, collaboration, planning and reporting capabilities.</span></p>
                        <div class="price mounth">Free forever</div>
                        <div class="price year">Free forever</div>
                        <a href="/join" class="btn start1">Start using</a>
                    </div>
                    <div class="features">
                        <ul>
                            <li>Unlimited projects / tasks / events</li>
                            <li>1 GB of storage</li>
                            <li>Up to 25 members</li>
                            <li>Calendar and events management</li>
                            <li>Custom workflows, task and project types</li>
                            <li>Time tracking</li>
                            <li>Analytics</li>
                            <li>Basic integrations (G-Suite, Google calendar, G-Mail, Google Drive, Email)</li>
                            <li>Activity stream</li>
                            <li>Dashboards</li>
                            <li>Views</li>
                            <li>Basic support</li>
                            <li>Customer success webinars</li>


                        </ul>
                    </div>
                </div>
                
            </div>
            <div class="c2">
                <div class="helper">
                    <div class="plan">
                        <h2><img class="price__icon" src="/site/assets/img/pricing/prof.png" alt="">Professional</h2>
                        <p class="users">5-250 users</p>
                        <p class="desc"><span class="desc__helper">Boost your bigger team's productivity with the GoodDay platform and grow even faster.</span></p>
                        <div class="price mounth"><span>$5</span> per user / month</div>
                        <div class="price year"><span>$4</span> per user / month</div>
                        <a href="/join" class="btn start2">Start using</a>
                    </div>
                    <div class="features">
                        <ul>
                            <li><strong>All Free edition features</strong></li>

                            <li>10 GB of storage</li>
                            <li>Up to 250 members</li>
                            <li>Advanced integrations (Slack, Dropbox, Box)</li>
                            <li>Advanced search</li>
                            <li>User roles customizations</li>
                            <li>Advanced views</li>
                            <li>Big Screens</li>
                            <li>Advanced time tracking</li>
                            <li>Reports and Advanced Analytics</li>
                            <li>Custom fields</li>
                            <li>Advanced timeline</li>
                            <li>Advanced task management (recurrency / duplicate / task templates)</li>
                            <li>24/7 support</li>
                            <li>One-to-one training</li>
                            <li>Dedicated customer success manager</li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="c3">
                <div class="helper">
                    <div class="plan">
                        <h2><img class="price__icon" src="/site/assets/img/pricing/enterprise.png" alt="">Enterprise</h2>
                        <p class="users">25-unlimited users</p>
                        <p class="desc"><span class="desc__helper">Best for large organizations, access to APIs, advanced security and compliance.</span> </p>
                        <div class="price">Request a quote</div>
                        <a href="mailto:sales@goodday.work" class="btn contact">Contact us</a>
                    </div>
                    <div class="features">
                        <ul>
                            <li><strong>All Professional edition features</strong></li>
                            <li>Unlimited storage</li>
                            <li>Unlimited members</li>
                            <li>Enterprise API</li>
                            <li>Advanced security features</li>
                            <li>SAML 2.0 Single Sign-On</li>
                            <li>Custom branding</li>
                            <li>Enrerprise access control</li>
                            <li>Private cloud option</li>
                            <li>Custom Reporting</li>
                            <li>24/7 support</li>
                            <li>One-to-one training</li>
                            <li>Dedicated customer success manager</li>

                        </ul>
                    </div>
                </div>


            </div>


        </block>


    </section>

<script>
    // $('.c1, .c2, .c3').equalHeights();
    // $('.desc').equalHeights();

$(function() {
    $(window).resize(function(){
        eqhe();
        eqhed();
    })
    eqhe();
    eqhed();
    $('.time__link').on('click', function(e){
        e.preventDefault();
        console.log($(this).attr('href').substr(1));
        $('section.pricing').removeClass('year mounth').addClass($(this).attr('href').substr(1));
        $('.time__link').removeClass('active');
        $(this).addClass('active');
    });

});

// equalheight
function eqhe(){
    if($(window).width()>1000){
        i = 0;
        height = [];
        $('.pricing .helper').each(function(){
            height[i] = $(this).height();
            i++;
        })
        $('.pricing .c1, .pricing .c2, .pricing .c3').height(Math.max.apply(null, height));
    }
    else{
        $('.pricing .c1, .pricing .c2, .pricing .c3').removeAttr('style');
    }
    
}

function eqhed(){
    if($(window).width()>1000){
        i = 0;
        height = [];
        $('.pricing .desc__helper').each(function(){
            height[i] = $(this).height();
            i++;
        });
        $('.pricing .desc').height(Math.max.apply(null, height));
    }
    else{
        $('.pricing .desc').removeAttr('style');
//        console.log($(window).width());
    }
}
</script>
<?php
require_once ('./site/pages/common/footer.php');
?>