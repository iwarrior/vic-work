<?php
require_once ('./site/pages/common/global_head.php');
require_once ('./site/pages/common/navigation.php');
?>

    <section class="header">
        <block>
            <div class="container">
                <h1>PRODUCT</h1>
                <div class="l2">Key features that power productive teams</div>
            </div>
        </block>
    </section>

    <section class="sub-nav">

        <block>

            <div class="container">

                <a href="javascript: product.goto('transparency');">Transparency</a> <span>|</span>
                <a href="javascript: product.goto('reports');">Reports</a> <span>|</span>
                <a href="javascript: product.goto('security');">Security</a> <span>|</span>
                <a href="javascript: product.goto('time-tracking');">Time tracking</a> <span>|</span>
                <a href="javascript: product.goto('integration');">Integrations</a> <span>|</span>
                <a href="javascript: product.goto('features');">Features</a>

            </div>

        </block>

    </section>


    <section class="section product poft" id="transparency">
        <block>

            <div class="left">
                <div class="container">
                    <img src="/site/assets/img/product/transp-ico.png" >
                    <h2>The Power of Transparency</h2>
                    <p class="desc">
                        Break up organizational silos and unlock the full potential of your team in an open,
                        collaborative environment.</p>
                </div>
            </div>

            <div class="right">

                <img src="/site/assets/img/product/transp-screen-2.jpg" class="snapshot" style="max-width: 600px;">
            </div>

        </block>
    </section>

    <section class="section product poft-more">
        <block>
            <div class="container">

                <p class="content">
                    From a single important task to a high-level overview of work in progress,
                    GoodDay automates transparency and visualizes all process flows and key data
                    for the stakeholders. Setup Big Screens in all common areas to quickly boost involvement, accountability, collaboration and productivity.

                </p>
                <img src="/site/assets/img/product/transp-2.png" style="max-width: 984px;" class="snapshot">

            </div>
        </block>
    </section>


    <section class="section product reports" id="reports">
        <block>
            <div class="left">
                <div class="container">
                    <div class="gd-icon-report"></div>
                    <h2>Automatic Reports</h2>
                    <p class="desc">GoodDay takes into account every work interaction and automatically generates detailed reports on progress, accomplishments,
                        collaboration quality and agility, eliminating the need for manual reporting.</p>
                </div>
            </div>
            <div class="right">
                <img src="/site/assets/img/product/transp-3.png" style="max-width: 371px;" class="snapshot">
            </div>
        </block>
    </section>

    <section class="section product reports-more">
        <block>
            <div class="left">
                <img src="/site/assets/img/product/reports-2.png" style="max-width: 487px;" class="snapshot">
                </div>
            <div class="right">
                    <div class="container">
                        <p class="desc">GoodDay comes with a set of out-of-the-box personal, project, department and organization-level  reports, which can be customized to show the data you need for any specified date range and scheduled for delivery to anyone in your team.</p>
                        <img src="/site/assets/img/product/reports-3.png" class="snapshot" style="max-width: 281px;">
                    </div>
                </div>
        </block>
    </section>


    <section class="product bi gray">
        <block>

            <img src="/site/assets/img/product/bi-ico.png" class="ico">
            <h1>Business Intelligence</h1>

            <p>
                GoodDay Business Intelligence Module backs up opinions about process quality and work success with facts and powers the decision makers with real-time data to boost performance.
                It offers a bird's eye view of all work activities as well as deep insights and analytics on progress and trends, helps improve estimates and forecasts, and allows to
                review all processes and uncover strategies for improvement.
            </p>

            <img src="/site/assets/img/product/bi-screens.png" class="snapshot" style="max-width: 799px;">

            <figure class="img-intelegense"></figure>
        </block>
    </section>



    <section class="product time-tracking">
        <block>
            <div class="gd-icon-time"></div>
            <h1>Time Tracking without tracking time</h1>
            <p class="content">GoodDay Business Intelligence module <b>automatically</b> tracks and analyzes all efforts even <b>without the traditional time reporting</b>.</p>
        </block>
    </section>

    <section class="product time-tracking-more" id="time-tracking">
        <block>

            <div class="column left">

                <div class="container">
                    <div class="ico">
                        <img src="/site/assets/img/product/tt-ico2.png">
                    </div>

                    <h1>Reported time</h1>
                    <p class="desc">If you require traditional time tracking, GoodDay offers built-in time reporting tools that don’t take time away from core work.</p>
                    <img src="/site/assets/img/product/tt-reported.png" class="screen">
                </div>
            </div>
            <div class="column right">
                <div class="container">

                    <div class="ico">
                        <img src="/site/assets/img/product/tt-ico3.png">
                    </div>

                    <h1>Estimated time</h1>
                    <p class="desc">Analyzing all activities for every user, GoodDay automatically estimates the efforts spent on each task, revealing the big picture.</p>
                    <img src="/site/assets/img/product/tt-estimated.png" class="snapshot" style="max-width: 439px;">
                </div>
            </div>
            <div class="cf"></div>

        </block>
    </section>

    <section class="product action-required">

        <block class="ar1">
            <div class="container">
                <img src="/site/assets/img/product/ar-ico.png" >
                <h1>Action Required</h1>
                <p class="desc">Action Required is a unique concept that makes GoodDay different from any other platform and enables its powerful features and business intelligence.</p>
                <p class="desc">Action Required identifies the team member responsible for the next action on each task. As a result, it enables high accountability, makes all process flows completely transparent, and serves as the foundation for many GoodDay features and analytics rooted in real-time work activity data.</p>
            </div>
            <img src="/site/assets/img/product/ar-timeline.png" class="timeline snapshot" style="max-width: 992px;">
        </block>


        <block class="accountability">

            <div class="column left">
                <div class="container">
                    <h2>100% Accountability</h2>
                    <p class="desc">With Action Required, responsibilities become clearly defined and transparent, which increases accountability across the organization.</p>
                </div>
            </div>
            <div class="column right">
                <div class="container">
                    <img src="/site/assets/img/product/ar-accountability.png" class="snapshot" style="max-width: 417px;">
                </div>
            </div>
        </block>



        <block class="eliminate">

            <div class="column left">
                <div class="container">
                    <img src="/site/assets/img/product/ar-eliminate.png" style="max-width: 358px;" class="snapshot">
                </div>
            </div>

            <div class="column right">
                <div class="container">
                    <h2>Eliminate Bottlenecks</h2>
                    <p class="desc">GoodDay is designed to reveal bottlenecks early, eliminate them quickly, and even to foresee the risk of a potential bottleck to disrupt the work process.</p>
                </div>
            </div>

            <div class="column left mobile">
                <div class="container">
                    <img src="/site/assets/img/product/ar-eliminate.png" style="max-width: 358px;" class="snapshot">
                </div>
            </div>

        </block>



        <block class="clear">

            <div class="column left">
                <div class="container">
                    <h2>Clear Goals</h2>
                    <p class="desc">With GoodDay planning features, everyone knows what to do and what the priorities are at any moment in time.</p>
                </div>
            </div>
            <div class="column right">
                <div class="container">
                    <img src="/site/assets/img/product/ar-clear.png" style="max-width: 343px;" class="snapshot">
                </div>
            </div>
        </block>

    </section>


    <section class="product security gray" id="security">
        <block>

            <h1>Security</h1>

            <div class="column">
                <div class="container">
                    <span class="img-encryption"></span>
                    <div class="title encryption">Data and File Encryption</div>
                    <ul class="content-smaller">
                        <li>All data is stored encrypted at rest</li>
                        <li>SSL/TLS encryption for all data in transit</li>
                        <li>256-bit AES file encryption</li>
                    </ul>
                </div>
            </div>
            <div class="column">
                <div class="container">
                    <span class="img-access-control"></span>
                    <div class="title access-control">Advanced Access Control</div>
                    <ul class="content-smaller">
                        <li>Strong authentication system</li>
                        <li>Integration with LDAP & Active Directory, Google sign-in</li>
                        <li>Centralized user management and access control</li>
                        <li>Flexible roles and permissions</li>
                    </ul>
                </div>
            </div>
            <div class="column">
                <div class="container">
                    <span class="img-data-centers"></span>
                    <div class="title data-centers">World-class Data Centers</div>
                    <ul class="content-smaller">
                        <li>99.9% uptime SLA</li>
                        <li>World class data centers</li>
                        <li>Audited data security controls</li>
                        <li>3-2-1 data backup</li>
                    </ul>
                </div>
            </div>

        </block>
    </section>


    <section class="product integration" id="integration">
        <block>

            <img src="/site/assets/img/product/i-ico.png">

            <h1><span>Email & Calendar</span> Integration</h1>

            <img src="/site/assets/img/product/i-screen.png" style="max-width: 600px;" class="snapshot">

            <p class="desc">GoodDay makes your work extra comfortable with its seamless integrations with the tools you already love. Use GoodDay with your Google email, calendar, and other apps and tools.</p>

            <div class="other-integretion">
                <div class="icon-integr-serv">
                    <span class="gd-ico"></span>
                    <span class="db-ico"></span>
                    <span class="gm-ico"></span>
                    <span class="sl-ico"></span>
                    <span class="oc-ico"></span>
                </div>
            </div>
        </block>
    </section>


    <section class="product more-fetatures" id="features">
        <block>

            <img src="/site/assets/img/product/f-ico.png">
            <h1>More features</h1>

            <div class="count">

                <div class="items-fetatures">
                    <div class="item-fetature">
                        <div class="count">
                            <span class="gd-icon-tasks"></span>
                            <h5>Tasks</h5>
                            <p>Flexible task and subtask structure allows creating the flows that fit your business processes.</p>
                        </div>
                    </div>
                    <div class="item-fetature">
                        <div class="count">
                            <span class="gd-icon-projects"></span>
                            <h5>Projects</h5>
                            <p>Create projects and subprojects with unlimited nesting levels and fully customizable workflows.</p>
                        </div>
                    </div>
                    <div class="item-fetature">
                        <div class="count">
                            <span class="gd-icon-view-tiles"></span>
                            <h5>Dashboards</h5>
                            <p>Build your ultimate personal custom dashboards with the information you really need and use.</p>
                        </div>
                    </div>
                    <div class="item-fetature">
                        <div class="count">
                            <span class="gd-icon-deadline"></span>
                            <h5>Planning</h5>
                            <p>Choose your way of planning work - a built-in calendar, visual timelines, or Gantt charts.</p>
                        </div>
                    </div>
                </div>
                <div class="items-fetatures">
                    <div class="item-fetature">
                        <div class="count">
                            <span class="gd-icon-status-board"></span>
                            <h5>Progress Board</h5>
                            <p>Organize work that you wish to monitor into boards for quick access and easy progress tracking.</p>
                        </div>
                    </div>
                    <div class="item-fetature">
                        <div class="count">
                            <span class="gd-icon-my-work"></span>
                            <h5>My Work</h5>
                            <p>Ultimate individual work planner with all tasks, milestones, and deadlines organized on one screen.</p>
                        </div>
                    </div>
                    <div class="item-fetature">
                        <div class="count">
                            <span class="gd-icon-calendar"></span>
                            <h5>Events</h5>
                            <p>Effectively plan and monitor project milestones and deadlines, department and personal events.</p>
                        </div>
                    </div>
                    <div class="item-fetature">
                        <div class="count">
                            <span class="gd-icon-report"></span>
                            <h5>Reports</h5>
                            <p>Personal, department, project, organization or custom reports are always ready to view or be delivered to your inbox.</p>
                        </div>
                    </div>
                </div>
                <div class="items-fetatures">
                    <div class="item-fetature">
                        <div class="count">
                            <span class="gd-icon-notification"></span>
                            <h5>Notifications</h5>
                            <p>Configure dashboard, email, and SMS notifications to receive updates that are important to you.</p>
                        </div>
                    </div>
                    <div class="item-fetature">
                        <div class="count">
                            <span class="gd-icon-search"></span>
                            <h5>Advanced Search</h5>
                            <p>Search with a click of a button and just a few characters to type.</p>
                        </div>
                    </div>
                </div>
                <div class="items-fetatures">
                    <div class="item-fetature">
                        <div class="count">
                            <span class="gd-icon-time"></span>
                            <h5>Time Tracking</h5>
                            <p>Automated effort estimation and manual time reporting capabilities.</p>
                        </div>
                    </div>
                    <div class="item-fetature">
                        <div class="count">
                            <span class="gd-icon-activity"></span>
                            <h5>Activity</h5>
                            <p>Real-time activity stream provides instant access to all statuses without update meetings.</p>
                        </div>
                    </div>
                </div>
                <div class="img-figure-book">
                    <figure class="img-mac-boock"></figure>
                </div>
            </div>
        </block>
    </section>


<?php
require_once ('./site/pages/common/footer.php');
?>