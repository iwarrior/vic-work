<?php


$inActionList = [

    11 => [
        'id' => 11,
        'title' => "Eliminate collaboration delays",
        'description' => 'Make collaboration process transparent to see what’s causing delays and improve accountability of your team members to get work done faster.',
        'image' => 'ia-11.jpg',
        'url' => 'collaboration-delays',

        'type'  => 'block',
        'picture'   => 'ia11.png',

        'tags'  => ['Accountability','Collaboration','Planning'],
        'bottomSeparator' => true
//        'separator' => true

    ],

    1 => [
        'id' => 1,
        'title' => "Where efforts go",
        'description' => 'Understand how your team spends their time with automatic time estimation module, know how much effort each project requires and use these insights to plan better.',
        'url' => 'where-efforts-go',
        'image' => 'ia-1.jpg',


        'type'  => 'block',
        'picture'   => 'ia1.png',

        'tags'  => ['Productivity', 'Time management', 'Planning'],
        'bottomSeparator' => false
    ],
    7 => [
        'id' => 7,
        'type'  => 'bg',
        'bgImg'   => 'ia7-bg.jpg',
        'picture' => 'ia7.jpg',
        'title' => "Lack of Accountability",
        'description' => 'Increase the accountability within your team with simple yet powerful Action Required concept that makes responsibilities clear and transparent to everyone.',
        'image' => 'ia-7.png',
        'url' => 'accountability',
        'tags'  => ['Behaviour','Accountability'],
        'bottomSeparator' => false
//        'separator' => false
    ],
    2 => [
        'id' => 2,
        'title' => "How to organize everyone",
        'description' => 'Get organized and stay organized with built-in planning tools, self-organization metrics, and complete transparency around everyone’s plans and priorities.',
        'image' => 'ia-2.png',
        'url' => 'how-to-organize-everyone',

        'type'  => 'block',
        'picture'   => 'ia2.png',
        'bottomSeparator' => true,

        'tags'  => ['Organization','Planning','Behaviour']
    ],
    4 => [
        'id' => 4,
        'type'  => 'block',
        'picture'   => 'ia4.png',
        'title' => "The power of :) and :(",
        'description' => 'Daily Badges reveal how well the team members follow important organizational processes and help make even routine tasks meaningful for everyone.',
        'image' => 'ia-4.png',
        'url' => 'teamboard',
        'tags'  => ['Behaviour', 'Motivation', 'Gamification'],
        'bottomSeparator' => true
    ],
    10 => [
        'id' => 10,
        'type'  => 'block',
        'picture'   => 'ia10.png',
        'title' => "Who and When",
        'description' => 'Minimize the number of status update meetings with real-time information about who is responsible for each next step on each task and when it is expected to be completed.',
        'image' => 'ia-10.png',
        'url' => 'who-and-when',
        'tags'  => ['Accountability','Planning'],
        'bottomSeparator' => true
    ],
    5 => [
        'id' => 5,
        'type'  => 'block',
        'picture'   => 'ia5.png',
        'title' => "How to solve performance issues",
        'description' => 'Addressing employee performance issues is one of the biggest challenges at work. Use the tools and  processes designed to reveal problems and help motivate team members.',
        'image' => 'ia-5.jpg',
        'url' => 'performance',
        'tags'  => ['Productivity','Behaviour'],
        'bottomSeparator' => false
    ],
    3 => [
        'id' => 3,
        'type'  => 'bg',
        'bgImg'   => 'ia3-bg.jpg',
        'picture' => 'ia3.jpg',
        'title' => "Revealing and preventing bottlenecks",
        'description' => 'Get the tools you need to help your team be agile and productive. Minimize bottlenecks that make business processes inefficient, cause delays and stress.',
        'image' => 'ia-3.png',
        'url' => 'bottlenecks',
        'tags'  => ['Planning','Collaboration']
    ],
    8 => [
        'id' => 8,
        'type'  => 'block',
        'picture'   => 'ia8.png',
        'title' => "Overwhelmed with tasks",
        'description' => 'Know who is overwhelmed and what the root cause is with real-time communication and collaboration analytics and visualizations.',
        'image' => 'ia-8.png',
        'url' => 'overwhelmed-with-tasks',
        'tags'  => ['Planning','Time management'],
        'bottomSeparator' => true
    ],
    6 => [
        'id' => 6,
        'type'  => 'block',
        'picture'   => 'ia6.png',
        'title' => "Clear goals and priorities for everyone",
        'description' => 'To make collaboration productive, use built-in best practices and automation that help set clear goals and priorities for everyone.',
        'image' => 'ia-6.gif',
        'url' => 'clear-goals-priorities',
        'tags'  => ['Planning','Productivity'],
        'bottomSeparator' => true
    ],
    9 => [
        'id' => 9,
        'type'  => 'block',
        'picture'   => 'ia9.png',
        'title' => "Focusing on success",
        'description' => 'Motivate team members and acknowledge great work every day, use Big Screens and Daily Badges to show what’s done and put accomplishments front and center.',
        'image' => 'ia-9.jpg',
        'url' => 'focusing-on-success',
        'tags'  => ['Motivation','Behaviour'],
        'bottomSeparator' => false
    ],
//    12 => [
//        'id' => 12,
//        'type'  => 'bg',
//        'bgImg'   => '',
//        'title' => "How to deliver on time",
//        'url' => 'deliver-on-time',
//        'description' => 'Solve the challenge of completing projects on time - enable good planning, accurate estimation and team accountability with GoodDay.',
//        'image' => 'ia-12.png',
//        'tags'  => ['Accountability','Collaboration','Planning']
//    ]
];

?>