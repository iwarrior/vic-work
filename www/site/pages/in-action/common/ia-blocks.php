<?php

function howHelpsBasic($title, $content, $className, $imgUrl) {

    ?>

    <section class="section in-action-section ia-block <?=$className?>">
        <block>
            <div class="column-left">
                <div class="count">
                    <div class="descr">
                        <h2><?=$title?></h2>
                        <?=$content?>
                    </div>
                </div>
            </div>
            <div class="column-right">
                <img src="<?=$imgUrl?>" class="hh-img" alt="<?=$title?>">
            </div>
        </block>
    </section>

    <?php



}


function iaHowHelpsImgOverBg($title, $content, $className) {

    ?>
    <section class="section in-action-section <?=$className?>">
        <div class="section-content">
            <div class="count-row gd-my-work">
                <div class="column-left">
                    <div class="count">
                        <div class="descr">
                            <h4><?=$title?></h4>
                            <?=$content?>
                        </div>
                    </div>
                </div>
                <div class="column-right">
                    <div class="count">
                        <figure class="gd-mywork-image"></figure>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php

}

function iaHowHelpsBigScreen($title, $content, $className) {

    ?>
    <section class="section in-action-section ia-block-bs <?=$className?>">
        <div class="section-content">
            <div class="count-row gd-priorities">
                <div class="column-left">
                    <div class="count">
                        <div class="descr">
                            <h4><?=$title?></h4>

                            <?=$content?>
                        </div>
                    </div>
                </div>
                <div class="column-right">
                </div>
            </div>
        </div>
    </section>
    <?php

}


function iaSeparator() {
    ?>

    <section class="ia-separator">
        <block></block>
    </section>

    <?php

}



function iaDescription($title,$colorClass,$class,$content,$hSize=2,$separator=true) {

    $separator = ($separator)?'border':'';
?>
    <section class="section in-action-section ia-block-desc">
        <block class="<?=$separator?>">
            <div class="column-left">
                <div class="count">
                    <div class="ia-section-title h<?=$hSize?> <?=$colorClass?>"><?=$title?></div>
                </div>
            </div>
            <div class="column-right">
                <div class="count">

                    <?=$content?>

                </div>
            </div>
        </block>
    </section>
<?php
}


function iaHowHelps($content,$class) {
    ?>
    <section class="section in-action-section ia-block-hh">
        <block>
            <div class="column-left">
                <div class="count">
                    <div class="ia-section-title h3 blue how-helps">How GoodDay Helps</div>
                </div>
            </div>
            <div class="column-right">
                <div class="summary">
                    <?=$content?>
                </div>
            </div>
        </block>
    </section>
    <?php
}





function iaHeader($data) {


    ?>

    <section class="header in-action">
        <block class="ia-list-item ia-<?=$data['id']?>">

            <div class="left">
                <img src="/site/assets/img/in-action/list2/<?=$data['picture']?>" alt="<?=$data['title']?>">
            </div>
            <div class="right">
                <a href="/in-action/" class="ia-back-list"><h2>In Action / </h2></a>
                <h1><?=$data['title']?></h1>
            </div>


        </block>



    </section>


    <?php

}






?>