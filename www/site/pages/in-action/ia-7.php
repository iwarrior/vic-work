<?php
$inActionId = 7;

require_once ('./site/pages/in-action/common/ia-blocks.php');
require_once ('./site/pages/in-action/common/head.php');
require_once ('./site/pages/in-action/common/list-config.php');

$inActionData = $inActionList[$inActionId];
iaHeader($inActionData);


?>




<?php

$content = <<<EOT
<p>While great teams rarely face this issue, not all employees feel accountable for everything they do at all times. Such attitude impacts the quality of their own deliverables, slows down the process and affects the team spirit. Increasing accountability across the whole team or organization is a very common challenge.</p>
EOT;
iaDescription("The Challenge",'red','ia7-d1',$content,2);




$content = <<<EOT
<p>Lack of accountability can destroy team spirit if you allow it to progress. To keep all team members productive, it is important to balance skills with workload while encouraging everyone to strive for their full potential. Being able to see accountability issues quickly and having the tools to prevent them is crucial for long-term success.</p>
EOT;
iaDescription("Long-term Impact",'purple','ia7-d1',$content,3);


$content = <<<EOT

<p>To increase accountability within the organization, team leaders should create an environment in which being accountable for one’s work is the only way to work.</p>
<p>Accountability becomes an issue when the responsibilities, contributions, and results are not clearly visible. If all work and processes are transparent, an employee that lacks accountability cannot rely on any of the common excuses for why work is not done well.</p>
<p>Transparency helps prevent accountability issues and address them if they arise - for each task, it is clear who it’s been with and for how long, so facts about performance and communication cannot be misrepresented.</p>
EOT;
iaDescription("Approaching the Problem",'yellow','ia7-d1',$content,4, false);








$content = <<<EOT
By providing the true picture of collaboration on any scale, helping with organization and planning, visualizing timelines, behavior metrics, priorities and goals,  GoodDay eliminates potential excuses and increases accountability.
EOT;
iaHowHelps($content,null);


$content = <<<EOT
<p>GoodDay visualizes all collaboration processes and gives a clear answer about who is or was the bottleneck and for how long.
Every team member has the tools to see the delays and act before their role in the task is affected.</p>
EOT;

howHelpsBasic("Waiting on a colleague",$content,'ia7-hh1','/site/assets/img/in-action/ia7/waiting.png');


iaSeparator();

$content = <<<EOT
<p>It is easy to see what everyone has been working on. GoodDay reveals the difference between work overload and procrastination or just not doing the job.</p>
EOT;

//howHelpsBasic("Too Much Work ",$content,'ia7-hh2','/site/assets/img/in-action/ia7/much.png');
howHelpsBasic("Being too busy",$content,'ia7-hh2','/site/assets/img/in-action/ia7/much.png');


iaSeparator();



$content = <<<EOT
<p>GoodDay prevents confusion around priorities with simple and effective prioritization tools. All tasks are easy to prioritize one by one, in My Work dashboard, or my Tasks. You can also set custom priorities to clarify the workflow.</p>
EOT;

//iaHowHelpsBigScreen("Misunderstood Priorities",$content,"ia2-hh-priorities");
howHelpsBasic("Misunderstood priorities",$content,'ia7-hh3','/site/assets/img/in-action/ia6/bs-priorities.png');

//$content = <<<EOT
//<p>GoodDay prevents confusion around priorities with simple and effective prioritization tools. All tasks are easy to prioritize one by one or in My Work dashboard or my Tasks. You can also set custom priorities to clarify the workflow.</p>
//EOT;
//
//howHelpsBasic("Misunderstood Priorities ",$content,'ia7-hh3','/site/assets/img/in-action/placeholder.png');
//iaSeparator();


$content = <<<EOT
<p>GoodDay makes it impossible for a task to be out of sight and out of mind because Action Required ensures all tasks are with a specific person at every point in time. In addition to My Work dashboard, reminders and email notifications can be set up to make sure all important work is top of mind.</p>
EOT;
//howHelpsBasic("No Lost or Forgotten tasks",$content,'ia7-hh4','/site/assets/img/in-action/ia7/forgotten.png');
howHelpsBasic("No lost or forgotten tasks",$content,'ia6-hh5','/site/assets/img/in-action/ia6/today.png');
iaSeparator();


$content = <<<EOT
<p>GoodDay, through Action Required, clearly indicates who is responsible for the next step in collaboration process. This is key for accountability, because every task at any moment in time is associated with just one person.</p>
EOT;

howHelpsBasic("Clear responsibilities",$content,'ia7-hh5','/site/assets/img/in-action/ia7/responsibilities.png');


?>


<?php
require_once ('./site/pages/in-action/common/foot.php');
?>