<?php
$inActionId = 10;

require_once ('./site/pages/in-action/common/ia-blocks.php');
require_once ('./site/pages/in-action/common/head.php');
require_once ('./site/pages/in-action/common/list-config.php');

$inActionData = $inActionList[$inActionId];
iaHeader($inActionData);


?>




<?php

$content = <<<EOT
<p>We juggle many projects and work with many people at once. These challenges with knowing who and when is working on a task are quite common:</p>
<p>
    <ul>
    <li>When many people collaborate on a task, it's hard so know who is working on a specific part at the moment, and miscommunication may also get in the way</li>
    <li>Plans and priorities change, and it’s often important to know quickly if a task you’re involved in has been updated or rescheduled by someone else</li>
    <li>Asking a colleague when he is planning to work on a task can waste time in communication cycles, so it would be great to know what’s going on without asking</li>
    <li>Confirming task status is often hard to do in the moment. Remote team members may be in different time zones and just cannot respond immediately</li>
    </ul>

</p>
EOT;
iaDescription("The Challenge",'red','ia10-d1',$content,2);




$content = <<<EOT
<p>Having access to real-time task updates to know who and when is planning to complete the work, saves time for all team members and creates an open environment in which everyone can plan their work effectively without waiting for direction. </p>
<p><strong>No micromanagement</strong></p>
<p>If all information about each task is always up to date and available, there is no need to constantly ask for details, plans, and ETAs.</p>
<p><strong>No status update meetings required</strong></p>
<p>Teams can save time by minimizing the number of meetings and emails to keep each other updated on tasks and projects. </p>
<p><strong>Improved accountability</strong></p>
<p>All data about tasks is open and all plans and outcomes are transparent. In this environment, team members feel more accountable for their work and its impact on the work of others.</p>
<p><strong>Smooth collaboration with remote teams</strong></p>
<p>Waiting for updates from a team member in another time zone may result in significant project delays, but real-time data helps eliminate this problem and speed up collaboration.</p>

EOT;
iaDescription("Why It Is Important",'purple','ia10-d1',$content,3, false);









$content = <<<EOT
<p>With action required, work planning tools and advanced notification system GoodDay provides full information on who is working on every task or project.</p>
EOT;
iaHowHelps($content,null);


$content = <<<EOT
<p>Because Action Required identifies the team member responsible for the next step on each task, at any point in time you can see who is working on what.</p>
EOT;
howHelpsBasic("Who really has to take action now",$content,'ia10-hh1','/site/assets/img/in-action/ia2/hh3.png');


iaSeparator();

$content = <<<EOT
<p>Because of built-in work planning, GoodDay knows not only who has to take the next step but also when it's scheduled for. </p>
EOT;

howHelpsBasic("When the task is scheduled for ",$content,'ia10-hh2','/site/assets/img/in-action/ia10/planning.png');

iaSeparator();


$content = <<<EOT
<p>Powered with an advanced notification system that collects all events and updates on the work you are involved in, GoodDay makes it really easy to select and deliver only the notifications that are important to you.</p>
EOT;

howHelpsBasic("Knowing what has changed",$content,'ia10-hh3','/site/assets/img/in-action/ia10/notifications.png');

iaSeparator();


$content = <<<EOT
<p>All team members have access to all and most recent information, without the need to wait for the next business day in another time zone.</p>
EOT;

howHelpsBasic("Remote teams are not excluded",$content,'ia10-hh4','/site/assets/img/in-action/ia10/remote.png');

iaSeparator();


$content = <<<EOT
<p>By organizing important projects and tasks into status boards, you can create a one-screen status summary view, which can be shared with everyone within GoodDay or on a Big Screen. Status boards are always up to date which means they provide a real-time picture and eliminate the need for requesting updates.</p>
EOT;

howHelpsBasic("Status boards",$content,'ia10-hh5','/site/assets/img/in-action/ia10/status-board.png');


?>


<?php
require_once ('./site/pages/in-action/common/foot.php');
?>