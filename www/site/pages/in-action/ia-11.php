<?php

$inActionId = 11;

require_once ('./site/pages/in-action/common/ia-blocks.php');
require_once ('./site/pages/in-action/common/head.php');
require_once ('./site/pages/in-action/common/list-config.php');

$inActionData = $inActionList[$inActionId];
iaHeader($inActionData);



$content = <<<EOT
<p>One of the factors that define consistently productive teamwork is the pace of collaboration. Effective teams are able to keep the pace even under pressure. However, keeping collaboration smooth and delay-free when working on multiple concurrent projects, coordinating the process across multiple departments, or managing remote teams is a common challenge. </p>
EOT;
iaDescription("The Challenge",'red','ia11-d1',$content,2);




$content = <<<EOT
<p>There are several common reasons why delays in collaboration occur.</p>

<p><b>Staying in the comfort zone.</b> Some organizations, especially the large and established, develop a stagnant culture in which working without the drive to act faster becomes the norm for everyone. In such environment, collaboration pace does not stand up to the challenges of competition and customer needs.</p>


<p><b>Too much work.</b> When we juggle too many tasks, the average speed with which we respond to each individual request slows down. Busy employees and managers delay collaboration when working in teams and become bottlenecks.</p>
<p><b>Misunderstood or unclear priorities.</b> Other team members are often not aware of how important a particular task is for the team to move forward and by deprioritizing it on their todo list slow down the whole team.</p>
EOT;
//<p><b>Lack of motivation to deliver faster.</b> In some cases, collaboration may be slowed down by a team member that works in their comfort zone and at the pace that is simply lower than the rest of the team's. While a delay of one day does not seem to make a difference to individual performance, for team collaboration this may create a snowball effect and disrupt the whole project.</p>
iaDescription("The Root Cause",'purple','ia11-d1',$content,3,false);







$content = <<<EOT
GoodDay acts as an invisible collaboration catalyst, makes sure at each point everyone knows who has to take action, reveals collaboration timeline and delays to all team members, summarizes data and motivates everyone to act faster.
EOT;
iaHowHelps($content,null);


$content = <<<EOT
<p>With Action Required, every task has a designated respondent. At every point in time, for every task,
everyone knows if they have to do something for a project to move on, which ensures personal accountability and process transparency for everyone involved.</p>
EOT;

howHelpsBasic("Next step accountability",$content,'ia11-hh1','/site/assets/img/in-action/ia11/accountability.png');


iaSeparator();

$content = <<<EOT
<p>With a complete picture of the whole collaboration process, all delays become visible to everyone. This can reveal what causes delays and motivates collaborators to improve the flow. Knowing where delays occur, who tends to become a bottleneck, who is overwhelmed with tasks, helps optimize the process for all future projects.</p>
EOT;

howHelpsBasic("Collaboration timeline",$content,'ia11-hh2','/site/assets/img/in-action/ia11/task-timeline.png');

iaSeparator();


$content = <<<EOT
<p>GoodDay pays special attention to making sure everyone knows what impact their work has on the organization performance as a whole. Knowing who is waiting for you and for how long helps prioritize work and motivates to improve reply time.</p>
EOT;

howHelpsBasic("See your impact",$content,'ia11-hh3','/site/assets/img/in-action/ia11/task-list.png');

iaSeparator();


$content = <<<EOT
<p>GoodDay processes all actions and provides metrics of teams’ agility, including how fast team members respond to each other. Metrics are included into regular reports and Big Screens.</p>
EOT;

howHelpsBasic("Collaboration agility",$content,'ia11-hh4','/site/assets/img/in-action/ia2/metrics.png');

iaSeparator();


$content = <<<EOT
<p>The powerful business intelligence module analyzes all collaboration processes behind the scenes to help GoodDay users and managers understand what affects collaboration quality. Breakdowns may occur on the individual user level when some team members consistently collaborate effectively with only certain colleagues but not others. On the cross-departmental level, analytics may reveal that some departments always delay the process, no matter what project they are involved in. </p>
<p>GoodDay visualizes these actionable insights and helps eliminate persistent issues that affect collaboration speed.</p>
EOT;

howHelpsBasic("Interpersonal and cross departmental ",$content,'ia11-hh5','/site/assets/img/in-action/ia11/interpersonal2.png');


?>


<?php
require_once ('./site/pages/in-action/common/foot.php');
?>