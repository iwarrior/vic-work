<?php
$inActionId = 3;

require_once ('./site/pages/in-action/common/ia-blocks.php');
require_once ('./site/pages/in-action/common/head.php');
require_once ('./site/pages/in-action/common/list-config.php');

$inActionData = $inActionList[$inActionId];
iaHeader($inActionData);


?>




<?php

$content = <<<EOT
<p>A bottleneck in collaboration process occurs when a team member has more tasks than he can complete and, as a result, prevents these or related tasks from moving forward.
Bottlenecks make business processes inefficient, cause delays and stress, and that's why solving this challenge is important for teams that strive to be agile and productive.</p>
EOT;
iaDescription("The Challenge",'red','ia3-d1',$content,2);




$content = <<<EOT
<p>Many organizational issues can cause bottlenecks, including poor planning, lack of resources, and inaccurate time estimates. But these causes are so common because of one major reason - lack of visibility and tools that help foresee that a team member is about to become a bottleneck.</p>
<p>There are two types of bottlenecks, and it is important to reveal and prevent both:</p>
<p><strong>Short-term bottlenecks</strong> are caused by temporary problems. An example is when a team member goes on vacation and no one else can take over their projects. This causes a backlog in their work until they return and may block the work of others.</p>
<p><strong>Long-term bottlenecks</strong> are rooted in process structure and occur repeatedly. An example is when only one team member in a department has the expertise to complete specific tasks, and becomes a bottleneck every time he gets too busy. </p>
EOT;
iaDescription("Why Bottlenecks Occur",'purple','ia3-d1',$content,4);


$content = <<<EOT
<p>The first step to unblocking bottlenecks and preventing them in the future is being able to identify them early and quickly. To do that, look for such symptoms as backlogged work, increased number of missed deadlines, and people waiting on each other for too long. </p>
<p>The second step is to communicate with all team members who are at risk of becoming a bottleneck or already are one, let them know when and how they block the process and affect the work of others, and resolve the root causes of delays.</p>
EOT;
iaDescription("Finding a Solution",'yellow','ia3-d1',$content,3, false);





$content = <<<EOT
With complete transparency around bottlenecks and analytics that allow to reveal the root causes,
GoodDay helps minimize the impact of user-defined blockers, uncover and eliminate hidden bottlenecks.
EOT;
iaHowHelps($content,null);


$content = <<<EOT
<p>
Users can mark tasks as Blockers to specify that delays on those tasks may prevent them from moving work forward.
This informs the team member responsible for the next action that he may be slowing down someone else’s work or becoming a bottleneck.
</p>
EOT;

howHelpsBasic("Identifying blockers",$content,'ia3-hh1','/site/assets/img/in-action/ia3/set_blocker.png');


iaSeparator();

$content = <<<EOT
<p>To let users know if they are slowing the process down and becoming a bottleneck, GoodDay allows to configure special notifications that can appear on My Work dashboard and can also be sent via email or text.</p>
EOT;

howHelpsBasic("Special notifications",$content,'ia3-hh2','/site/assets/img/in-action/ia3/notifications.png');

iaSeparator();


$content = <<<EOT
<p>
GoodDay reveals bottlenecks even if tasks are not explicitly marked as Blockers. Analyzing who is causing workflow delays within the team as a whole and on the project level, the system provides collaboration agility and average reply time metrics to show where the bottlenecks are.
</p>
EOT;

howHelpsBasic("Revealing hidden bottlenecks",$content,'ia3-hh3','/site/assets/img/in-action/ia3/hidden.png');

iaSeparator();


$content = <<<EOT
<p>Big Screen with tasks overview displays a real-time big picture of how the workload is distributed between team members. Everyone can see at a glance if bottlenecks occur and can take action to eliminate them.</p>
EOT;

howHelpsBasic("Real-time transparency",$content,'ia3-hh4','/site/assets/img/in-action/ia3/big-screen.png');

iaSeparator();


$content = <<<EOT
<p>GoodDay business intelligence module analyzes all actions and workload distribution to provide insight into bottlenecks, reveal them early, and help reorganize and improve processes to prevent future problems.</p>
EOT;

howHelpsBasic("Advanced analytics",$content,'ia3-hh5','/site/assets/img/in-action/ia3/analytics.png');


?>


<?php
require_once ('./site/pages/in-action/common/foot.php');
?>