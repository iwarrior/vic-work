<?php

$inActionId = 1;


require_once ('./site/pages/in-action/common/ia-blocks.php');
require_once ('./site/pages/in-action/common/list-config.php');

require_once ('./site/pages/in-action/common/head.php');

$inActionData = $inActionList[$inActionId];


?>









<?php

iaHeader($inActionData);


$content = <<<EOT
<p>Knowing where efforts go is crucial for planning, performance evaluation, and time-management improvement. Understanding how your team spends their time allows to fine-tune short term priorities and helps develop an effective long-term work plan.</p>
<p>To keep track of where efforts go, it is common to implement timesheet solutions or manual reports. However, these approaches have significant drawbacks - they are time-consuming and are often perceived as a waste of time and micromanagement. </p>
EOT;
iaDescription("The Challenge",'red','ia1-d1',$content,2,true);




$content = <<<EOT
                        <p>Lack of visibility into past efforts leads to negative outcomes:</p>
                        <ul>
                            <li>No reliable data on what it takes to complete a project </li>
                            <li>Planning for the future becomes guesswork</li>
                            <li>No clear picture of productivity i.e. efforts spent vs. results achieved</li>
                            <li>Conflicting opinions about each other’s productivity and performance among team members</li>
                        </ul>
EOT;
iaDescription("Impact",'purple','ia1-d1',$content,2,false);



$content = <<<EOT
Automatically estimates all the efforts without time reports, provides user friendly tools to report time, if needed, and creates great data visualisation for analysis.
EOT;
iaHowHelps($content,null);


$content = <<<EOT
<p>GoodDay business intelligence module collects all data, analyzes activities and history, and
estimates the amount of effort users spend on tasks. While not every task may be estimated with
a 100% accuracy in the beginning, over time the estimates become accurate and reflect the exact
amount of effort.</p>
EOT;

howHelpsBasic("Time is tracked automatically",$content,'ia1-hh1','/site/assets/img/in-action/ia1/hh1.png');


iaSeparator();

$content = <<<EOT
<p>See a high-level overview of where the user’s or team’s efforts go with real-time, visual analytics.
Go deeper and analyze each project individually to find ways to improve productivity or better
align actual work with your changing priorities.</p>
EOT;

howHelpsBasic("Complete picture at a glance",$content,'ia1-hh2','/site/assets/img/in-action/ia1/hh2.png');

iaSeparator();


$content = <<<EOT
<p>If you business requires exact time reporting, GoodDay offers a powerful built-in time reporting module
which allows users to track time they spend on projects and tasks without being distracted from their core work. </p>
EOT;

howHelpsBasic("Seamless time reporting",$content,'ia1-hh3','/site/assets/img/in-action/ia1/hh3.png');

iaSeparator();


$content = <<<EOT
<p>To see the big picture of what’s been achieved within a specific period of time, use What’s Done Report which
pulls all required information, for an individual user or a whole team, into one screen for a real-time, clear view. </p>
EOT;

howHelpsBasic("What’s done",$content,'ia1-hh4','/site/assets/img/in-action/ia1/hh4.png');

iaSeparator();


$content = <<<EOT
<p>
    GoodDay offers powerful, easy-to-use reports that allow let you analyze where efforts with a click of a button.
    Among other insights, you can slice and dice work data by user, project, any time period, see how much time each
    user logged manually and how much GoodDay automation estimated was spent on a project.
</p>
EOT;

howHelpsBasic("Detailed reports",$content,'ia1-hh5','/site/assets/img/in-action/ia1/hh5.png');


?>


<?php
require_once ('./site/pages/in-action/common/foot.php');
?>