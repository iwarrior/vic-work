<?php
$inActionId = 2;

require_once ('./site/pages/in-action/common/ia-blocks.php');
require_once ('./site/pages/in-action/common/head.php');
require_once ('./site/pages/in-action/common/list-config.php');

$inActionData = $inActionList[$inActionId];
iaHeader($inActionData);

?>




<?php

$content = <<<EOT

<p>Being organized is part of being productive. Knowing that everything is under control and in its place helps achieve more, do it faster and with less resources.</p>
<p>Good work organization includes several aspects:</p>
<ul>
<li>Everyone knows what to do</li>
<li>The team has a specific and time-bound plan</li>
<li>Priorities are set and clear for everyone</li>
<li>Workload is distributed evenly, with no extreme busy or low times</li>
<li>No tasks are “lost” or “forgotten”, etc.</li>
</ul>

<p>While some people may have good organization skills when working on their own, it is challenging to make and keep the whole team organized.</p>
EOT;
iaDescription("The Challenge",'red','ia2-d1',$content,2);




$content = <<<EOT
<p>Staying organized may be even more challenging than getting things together in the first place. While to get organized it may be enough to sit down once and put priorities and schedules in order, keeping up to the organization standard is an ongoing effort:</p>
<ul>
    <li>Priorities change and plans need to be revised</li>
    <li>Work environments become more and more fast-paced, and people on one team are not in sync with each other at all times</li>
    <li>Some people have better organization skills than others</li>
    <li>Staying organized takes time away from work, for example, to hold regular meetings with the team to get on the same page. </li>
</ul>

<p>Often, there are no clear warning signs of things starting to fall apart. Disorganization creeps up slowly and one day it’s too late to quickly adjust the process, the organization exercise has to be repeated all over again.</p>
EOT;
//<p>GoodDay offers great features that help getting perfectly organized. Automation, motivation tactics and planning tools will keep everyone organized in the long run. </p>
iaDescription("Staying Organized",'purple','ia2-d1',$content,3,false);







$content = <<<EOT
Designed to keep everyone organized by default without extra effort, GoodDay encourages staying organized and reveals issues with organization if they occur.
EOT;
iaHowHelps($content,null);


$content = <<<EOT
<p>GoodDay tools and approach to planning ensure that work is perfectly organized and that all plans are aligned with priorities. While planning and scheduling every task is not mandatory, the system is designed to push users towards perfect planning every day.</p>
EOT;

howHelpsBasic("Effective built-in planning",$content,'ia2-hh1','/site/assets/img/in-action/ia2/hh1.png');

iaSeparator();

$content = <<<EOT
<p>Action Required and built-in planning ensure that all plans are clearly defined and up to date. This makes it possible to create the ultimate start-of-the day dashboard - My Work. My Work shows what you need to do now, your plans, milestones and deadlines all on one screen. Team members can access each others’ s My Work screens as well, to see everyone’s workload and priorities.</p>
EOT;
//iaHowHelpsImgOverBg("My Work",$content,'ia2-hh2');
howHelpsBasic("My Work",$content,'ia2-hh2','/site/assets/img/in-action/ia2/my-work.png');


iaSeparator();

$content = <<<EOT
<p>Priorities are clear for every user on the task and project level and transparent for all team members. They can also be customized to provide a more granular view into your specific workflows and reports.</p>
<figure class="gd-priorities-img"></figure>
EOT;

//iaHowHelpsBigScreen("Clear priorities",$content,"ia2-hh-priorities");
howHelpsBasic("Clear priorities",$content,'ia2-hh-priorities','/site/assets/img/in-action/ia6/bs-priorities.png');

iaSeparator();



$content = <<<EOT
<p>Action Required is a unique feature that may seem small, but serves as the foundation for the big data analytics behind GoodDay. Every task at any moment in time is associated with a person responsible for the next action. While this also makes many other GoodDay features so powerful, Action Required ensures high accountability and transparency around all process flows.</p>
EOT;

howHelpsBasic("Action Required & accountability",$content,'ia2-hh3','/site/assets/img/in-action/ia2/hh3.png');

iaSeparator();

$content = <<<EOT
<p>Big Screens reveal current organization challenges and demonstrate accomplishments to inspire. There are designated screens to help with setting priorities, planning, behaviour management and, of course, self-organization.</p>
EOT;

//iaHowHelpsBigScreen("Transparency around organization",$content,"ia2-hh-transparency");
howHelpsBasic("Transparency around organization",$content,"ia2-hh-transparency",'/site/assets/img/in-action/ia2/transparency.jpg');

iaSeparator();

$content = <<<EOT
<p>Behind the scenes, GoodDay analyzes all user actions and enables self-organization metrics that clearly show how well organized the user is or if things go wrong. GoodDay displays these metrics throughout the application which creates transparency that pushes everyone to improve.</p>
EOT;

howHelpsBasic("Self-organization metrics",$content,'ia2-metrics','/site/assets/img/in-action/ia2/metrics.png');


?>


<?php
require_once ('./site/pages/in-action/common/foot.php');
?>