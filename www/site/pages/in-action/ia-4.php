<?php
$inActionId = 4;

require_once ('./site/pages/in-action/common/ia-blocks.php');
require_once ('./site/pages/in-action/common/head.php');
require_once ('./site/pages/in-action/common/list-config.php');

$inActionData = $inActionList[$inActionId];
iaHeader($inActionData);

?>




<?php

$content = <<<EOT
<p>People may not feel inspired by following organizational processes or completing day-to-day routine tasks, although they are essential for progress. To keep teams excited about everything they do, including seemingly unimportant tasks, GoodDay offers a simple yet effective solution, Daily Badges.</p>
EOT;
iaDescription("The Challenge",'red','ia4-d1',$content,2);




$content = <<<EOT
<p>For every team member, you can set up rules or responsibilities to fulfill in order to receive a happy face Daily Badge. If the user does not fulfill the requirements, he receives a sad face badge. The badges are displayed on the individual Dashboards, within reports, and on Big Screens.</p>
<p>There are many built-in rules available and custom rules can be created and enabled through GoodDay API to reflect the organization’s unique requirements. </p>
EOT;
iaDescription("The Daily Badges",'purple','ia4-d1',$content,3);


$content = <<<EOT
<p>Daily Badges are a great tool to acknowledge good attitude towards work and motivate the teams. For example, a happy face badge is given to a user every day when the user stays organized, there are no past due tasks or events, all work is planned and Inbox is empty. Completing day-to-day tasks can also be rewarded, including timely execution of recurring tasks, submitting the deliverables daily into a central repository, reporting time or being at work on time every day.</p>
<p><Daily Badges also help solve performance and organizational challenges. The challenges can be defined and integrated into the system, and GoodDay will provide a clear view on whether the issue is getting fixed or not. /p>
EOT;
iaDescription("Recognition & Problem Solving",'yellow','ia4-d1',$content,4,false);





$content = <<<EOT
Through encouraging and publicly acknowledging positive attitude and good work ethic, GoodDay makes sure that great habits are rewarded and thrive and poor work practices are eliminated over time.
EOT;
iaHowHelps($content,null);


$content = <<<EOT
<p>All team members know which important tasks or routines the happy face badge is awarded for. Organized into the Team Board and being a part of your Big Screen, Daily Badges are displayed to everyone to create forced transparency which helps improve user behaviour. People will work harder to receive a happy face badge, because it demonstrates to all their colleagues that they follow important organizational processes.</p>
EOT;

howHelpsBasic("Higher accountability",$content,'ia4-hh1','/site/assets/img/in-action/ia5/tb.png');


iaSeparator();

$content = <<<EOT
<p>In addition to company-wide, common rules that determine which badge is awarded, you can set up custom criteria and requirements for each team member individually. These requirements may be based on the user’s position, responsibilities or personal self-improvement goals.</p>
EOT;

howHelpsBasic("Personalized rules ",$content,'ia4-hh2','/site/assets/img/in-action/ia4/rules.png');

iaSeparator();


$content = <<<EOT
<p>Managers and team members always have visibility into the behavior and processes that determine which Daily Badges are given to each user. If a problem exists, GoodDay reveals it quickly and provides a clear view into what may be causing the problem.</p>
EOT;

howHelpsBasic("At-a-glance visibility",$content,'ia4-hh3','/site/assets/img/in-action/ia4/visibility.png');

iaSeparator();


$content = <<<EOT
<p>Daily Badges functionality can be extended with GoodDay API. You can connect to any 3rd party solution and implement custom rules that fit your business processes and reflect the goals you are trying to reach.</p>
EOT;

howHelpsBasic("API for custom solutions",$content,'ia4-hh4','/site/assets/img/in-action/ia4/api.png');

iaSeparator();


$content = <<<EOT
<p>Being well-organized helps do your job better. Seeing that you are well organized on the Big Screen is a positive motivator. Knowing that other team members also see and acknowledge your success, is an even stronger motivator.</p>
EOT;

howHelpsBasic("Fun and positive attitude",$content,'ia4-hh5','/site/assets/img/in-action/ia4/team.jpg');


?>


<?php
require_once ('./site/pages/in-action/common/foot.php');
?>