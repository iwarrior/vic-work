<?php
$inActionId = 9;

require_once ('./site/pages/in-action/common/ia-blocks.php');
require_once ('./site/pages/in-action/common/head.php');
require_once ('./site/pages/in-action/common/list-config.php');

$inActionData = $inActionList[$inActionId];
iaHeader($inActionData);


?>




<?php

$content = <<<EOT
<p>Building an engaged team is as important as finding talented employees with the right skill set.
When employees are fully dedicated to and enthusiastic about their work, work outcomes improve dramatically.
Research also confirms that intrinsic motivation impacts performance much more than extrinsic. </p>
EOT;
iaDescription("The Challenge",'red','ia9-d1',$content,2);


$content = <<<EOT
<p>In today’s knowledge-based economy, people are the main source of innovation and business growth. When employees are engaged, they are more productive,
more innovative, take initiative and have a positive attitude to help organizations achieve their goals. When focusing on engagement, managers increase
productivity by creating an environment that energizes and motivates teams.</p>
<p>Focusing on success and acknowledging all work contributions is a powerful way to keep employees engaged.</p>
EOT;
iaDescription("Why It Is Important",'purple','ia9-d1',$content,3, false);

//$content = <<<EOT
//<p>There are several factors that impact our workload:</p>
//<p>
//    <ul>
//        <li>the number of tasks we create for ourselves or receive from colleagues</li>
//        <li>how many tasks we complete</li>
//        <li>how many tasks we are involved in and spend time contributing to</li>
//        <li>who we communicate with and what their workload is like (is this what we are saying with last screen?)</li>
//    </ul>
//</p>
//<p>In GoodDay, there are metrics and visualizations that allow analyzing all these factors for every user. Our detailed collaboration and communication analytics help detect who is overwhelmed with tasks and help improve the situation.</p>
//EOT;
//iaDescription("The Root Cause",'purple','ia9-d1',$content,3);
//
//
//






$content = "<p>Automatically shows improvements in different areas, summarizes work accomplishments, helps acknowledge achievements across the organization.</p>";
iaHowHelps($content,null);


$content = <<<EOT
<p>To acknowledge progress and motivate teams to improve further, GoodDay visualizes the analytics that illustrate key positive changes. It analyzes activity of all users and automatically develops performance and success metrics which are then compared to the previous periods to highlight improvements and accomplishments. </p>
EOT;
howHelpsBasic("Last week’s improvements",$content,'ia9-hh1','/site/assets/img/in-action/ia9/improvements.png');
iaSeparator();

$content = <<<EOT
<p>In addition to built-in recognition features, GoodDay allows to manually create custom Big Screens where anyone can acknowledge great work, individual contributions,
 or successes of the team in a particular project.</p>
EOT;
howHelpsBasic("Thank You board",$content,'ia9-hh2','/site/assets/img/in-action/ia9/highlights.png');
//iaSeparator();


$content = <<<EOT
<p>To create complete transparency around what’s been accomplished within a week or a month, GoodDay offers a detailed What’s Done report that can be scheduled and emailed to users
or set as a Big Screen. It provides an additional way to recognize great work - all closed tasks, completed projects and reached milestones - and all team members that were involved.</p>
EOT;
howHelpsBasic("What’s done",$content,'ia9-hh3','/site/assets/img/in-action/ia9/rep-done.png');


?>


<?php
require_once ('./site/pages/in-action/common/foot.php');
?>