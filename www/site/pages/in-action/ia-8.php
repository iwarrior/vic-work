<?php
$inActionId = 8;

require_once ('./site/pages/in-action/common/ia-blocks.php');
require_once ('./site/pages/in-action/common/head.php');
require_once ('./site/pages/in-action/common/list-config.php');

$inActionData = $inActionList[$inActionId];
iaHeader($inActionData);


?>




<?php

$content = <<<EOT
<p>Sometimes we get so busy at work that, day after day, we feel completely overwhelmed with tasks. When this happens, it is important to recognize the
 problem and address it to stay motivated and productive without unnecessary stress.</p>

<p>It's hard to stay productive when workload gets out of control. Being able to see the root cause of overwhelm and to effectively resolve the issue is
a common challenge for team members and leaders.</p>
EOT;
iaDescription("The Challenge",'red','ia8-d1',$content,2);




//$content = <<<EOT
//<p>Several key factors impact our workload:</p>
//<p>
//    <ul>
//        <li>the number of tasks we create for ourselves or receive from colleagues</li>
//        <li>how many tasks we complete</li>
//        <li>how many tasks we are involved in and spend time contributing to</li>
//        <li>who we communicate with and what their workload is like</li>
//    </ul>
//</p>
//<p>GoodDay high-level metrics and visualizations allow analyzing all these factors for every user, while
//detailed collaboration and communication analytics help detect who is overwhelmed with tasks and help improve the situation.</p>
//EOT;
//iaDescription("The Root Cause",'purple','ia8-d1',$content,3,false);
//





$content = <<<EOT
<p>There are many reasons why we get overwhelmed with work, including poor planning, not enough team members to share work with and delegate to, misalignment in skills and assigned tasks, or an influx of new ideas to work on.</p>
<p>To be able to prevent the overwhelm and optimize workload distribution, start by carefully and honestly analyzing  all factors that influence how much work you have at any given moment. Look at where the tasks come from, how much work gets done, where the tasks move next, and how much time is spent in communication with each collaborator. This analysis will help pinpoint sources of overload and uncover opportunities to delegate and improve work distribution.</p>

EOT;
iaDescription("The Approach",'purple','ia8-d1',$content,3,false);





$content = <<<EOT
Visualizes the workflow, estimates effort distribution by project and team member,
and suggests new, more optimal work distribution and delegation opportunities.
EOT;
iaHowHelps($content,null);


$content = <<<EOT
<p>Know who the tasks are coming from and how long it takes each user to reply.
You may find that while 40% of user’s tasks come from colleague A, while the average reply time to A is very short.
This indicates that colleague A is not necessarily causing high workload. To find out who is overwhelmed and the sources of those tasks,
GoodDay allows analyzing the average time to reply for each user and comparing metrics between team members in similar roles.</p>
EOT;

howHelpsBasic("Detect the workload source",$content,'ia8-hh1','/site/assets/img/in-action/ia8/from.png');


iaSeparator();

$content = <<<EOT
<p>See what typically happens to tasks assigned to a user who’s feeling overwhelmed. Some may be getting done (and closed), but many may just be passed on to other users. If tasks often just move from user A to user B very quickly, it may be possible to exclude user A from this collaboration chain and save him some time.</p>
EOT;

howHelpsBasic("See where the workload moves",$content,'ia8-hh2','/site/assets/img/in-action/ia8/to.png');

iaSeparator();


$content = <<<EOT
<p>To help users who feel overwhelmed, analyze reply time and time taken to complete by user,
task and project. Each user may be involved into a task as its creator, someone responsible to complete it,
or as a collaborator. These roles are visible in GoodDay and can be analyzed to shift workload from the overwhelmed user who is responsible for too much actual execution work.</p>
EOT;

howHelpsBasic("The type of work everyone is involved in",$content,'ia8-hh3','/site/assets/img/in-action/ia8/stats.png');

iaSeparator();


$content = <<<EOT
<p>GoodDay helps reviewing daily activity with visual day-by-day summaries and infographics that reveal the amount of effort a user or a team dedicated to a particular project.
These visualizations allow to catch work overload quickly and early and immediately redistribute the tasks in a more efficient way.</p>
EOT;

howHelpsBasic("Analyze day to day activity",$content,'ia8-hh4','/site/assets/img/in-action/ia8/activity2.png');

iaSeparator();


$content = <<<EOT
<p>To explain how users communicate and collaborate with each other to get work done, GoodDay visualizes all process flows.
This granular view allows to analyze if some users tend to respond to others slowly, which may be a sign of work overload. </p>
EOT;

howHelpsBasic("Visualize communication flows",$content,'ia8-hh5','/site/assets/img/in-action/ia11/interpersonal2.png');


?>


<?php
require_once ('./site/pages/in-action/common/foot.php');
?>