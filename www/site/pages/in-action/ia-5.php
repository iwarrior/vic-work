<?php
$inActionId = 5;

require_once ('./site/pages/in-action/common/ia-blocks.php');
require_once ('./site/pages/in-action/common/head.php');
require_once ('./site/pages/in-action/common/list-config.php');

$inActionData = $inActionList[$inActionId];
iaHeader($inActionData);


?>




<?php

$content = <<<EOT
<p>
Addressing employee performance issues is one of the biggest challenges for managers. It is hard to help an employee improve without tools and processes that could reveal performance challenges, help motivate and track positive change.
</p>
EOT;
iaDescription("The Challenge",'red','ia5-d1',$content,2);




$content = <<<EOT
<p><strong>Step 1: Clearly state the problem and back it up with data.</strong> </p>
<p>When bringing up an employee performance issue, make sure you are presenting not only an opinion, but demonstrate supporting data metrics, KPIs, and trends.</p>
<p><strong>Step 2: Set goals and establish success metrics.</strong> </p>
<p>"Getting better" can be treated differently by different people. A clear goal along with progress tracking metrics will help quantify the concept of improvement.</p>
<p><strong>Step 3: Track progress.</strong> </p>
<p>Develop a personal progress report and discuss the results during regular one-on-one meetings. Agree on and document the next week’s challenges, acknowledge improvements and identify areas for development.</p>
<p>GoodDay is designed to improve performance over time and offers tools that help implement this 3-step strategy from start to success.</p>
EOT;
iaDescription("The Approach",'purple','ia5-d1',$content,2, false);



$content = <<<EOT
Performance improvement strategies are supported with transparency, planning and accomplishment monitoring tools,
behavioral metrics that reveal issues and demonstrate progress, and the principle of positive phychology that improves motivation.
EOT;
iaHowHelps($content,null);


$content = <<<EOT
<p>GoodDay provides a comprehensive set of metrics, reports and visualizations to analyze all aspects of performance.
This ensures that managers can approach employee performance issues with complete information and base the improvement plan on facts and observable trends.</p>
EOT;

howHelpsBasic("Clearly identify issues",$content,'ia5-hh1','/site/assets/img/in-action/ia5/performance.png');


iaSeparator();

$content = <<<EOT
<p>This module allows to define and document performance improvement goals and track the history of set goals and accomplishments.</p>
EOT;

howHelpsBasic("Personal improvement goals",$content,'ia5-hh2','/site/assets/img/in-action/ia5/set-goals.png');

iaSeparator();


$content = <<<EOT
<p>During one-on-one meetings, GoodDay helps reviewing last week’s accomplishments and clearly shows if performance has improved.
If a metric agreed upon as a focus for improvement has not changed, this will be clearly visible within GoodDay analytics.</p>
EOT;

howHelpsBasic("Challenges & progress are easy to review",$content,'ia5-hh3','/site/assets/img/in-action/ia5/review-goals.png');

iaSeparator();


$content = <<<EOT
<p>When working through performance issues, GoodDay behavioral metrics also offer a great starting point and useful progress indicators to monitor.</p>
EOT;

howHelpsBasic("Trends in key behavioral metrics",$content,'ia5-hh4','/site/assets/img/in-action/ia5/dashboard.png');

iaSeparator();


$content = <<<EOT
<p>Daily Badges are often a great motivator. You can set up responsibilities to fulfill or metrics to reach as prerequisites to receiving a happy face Daily Badge. The Badges can be displayed on the individual Dashboard, within reports, and on the Big Screens.</p>
EOT;

howHelpsBasic("Transparency & gamification improve performance",$content,'ia5-hh5','/site/assets/img/in-action/ia5/tb.png');


//$content = <<<EOT
//<p>Daily Badges are often a great motivator. You can set up responsibilities to fulfill or metrics to reach according to performance improvement plan as prerequisites to receiving a happy face Daily Badge. The Badges can be displayed on the individual Dashboard, within reports, and on the Big Screens</p>
//EOT;
//
//iaHowHelpsBigScreen("Transparency & Gamification Improves Performance",$content,"ia5-hh-team-board");



?>


<?php
require_once ('./site/pages/in-action/common/foot.php');
?>