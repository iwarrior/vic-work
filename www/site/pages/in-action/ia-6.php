<?php
$inActionId = 6;

require_once ('./site/pages/in-action/common/ia-blocks.php');
require_once ('./site/pages/in-action/common/head.php');
require_once ('./site/pages/in-action/common/list-config.php');

$inActionData = $inActionList[$inActionId];
iaHeader($inActionData);
?>




<?php

$content = <<<EOT
<p>
Without clear goals and priorities, it's hard for team members to know what to work on next and why. As a result, deliverables
may not align with the project goals or overall business strategy. It's a challenge to keep all teams informed and in sync,
especially when working on multiple projects and when priorities change. Setting clear goals and making sure that work is aligned with priorities is key to productive collaboration.
</p>
EOT;
iaDescription("The Challenge",'red','ia6-d1',$content,2, false);




$content = <<<EOT





<p>The following strategies can help teams of any size set clear goals and priorities:</p>
<p>
    <ul>

        <li>Separate long-term goals from short-term goals to plan and prioritize effectively</li>
        <li>Clearly define each goal, make sure it is realistic and measurable</li>
        <li>Set goals and priorities together with the teams that will work on them</li>
        <li>Write all goals and priorities down</li>
        <li>Communicate goals and priorities to everyone, including all changes and updates</li>
        <li>Break goals down into milestones or tasks and track each accomplishment</li>
        <li>Review goals, priorities and progress regularly</li>
        <li>Stick to goals, be accountable, but allow for flexibility to ensure goals still align with evolving business needs and vision</li>
    </ul>
</p>
<p>GoodDay was created with these best practices in mind and offers many tools that enable and automate them.</p>
<p></p>
EOT;
iaDescription("Best Practices",'purple','ia6-d1',$content,2, false);



$content = <<<EOT
Brings planning, goals, priorities and execution together into one space where monitoring progress towards goals is automated and is always based on complete and up-to-date information.
EOT;
iaHowHelps($content,null);


$content = <<<EOT
<p>Set up a Big Screen to visualize what was planned, completed, is still in progress, or has been rescheduled.
This screen will show a high-level overview of current goals and priorities and will also help see any issues that require immediate attention and review.</p>
EOT;

howHelpsBasic("Goals, plans, and reality",$content,'ia6-hh1','/site/assets/img/in-action/ia6/task-board.png');


iaSeparator();

$content = <<<EOT
<p>
Priorities Big Screen provides an overview of the team's current project and task goals. When all priorities are visible, every team member has the necessary information about business goals to align their individual work in an optimal way.
</p>
EOT;

howHelpsBasic("Current priorities",$content,'ia6-hh2','/site/assets/img/in-action/ia6/bs-priorities.png');

iaSeparator();


$content = <<<EOT
<p>It may be a good idea to customize the list of default priorities if you need to adapt the system to your workflow requirements, need a more granular view, or simply need to find the right words to emphasize the importance of tasks and projects.</p>
EOT;

howHelpsBasic("Custom priorities",$content,'ia6-hh3','/site/assets/img/in-action/ia6/custom.png');

iaSeparator();


$content = <<<EOT
<p>
Setting up your goals as milestones, events, and defining deadlines, is a good tactic to make goals measurable and time-bound.
GoodDay clearly shows if milestones and deadlines were reached or missed, and this helps understand how well users or teams are moving towards set goals.</p>
EOT;

howHelpsBasic("Milestones, events and deadlines",$content,'ia6-hh4','/site/assets/img/in-action/ia6/milestones.png');

//iaSeparator();


$content = <<<EOT
<p>My Work dashboard allows everyone to manage their priorities on a tactical, micro-level - for each day. This view can be shared with other team members to make sure daily priorities are aligned for the whole team.</p>
EOT;

howHelpsBasic("Plans and priorities for today",$content,'ia6-hh5','/site/assets/img/in-action/ia6/today.png');


?>


<?php
require_once ('./site/pages/in-action/common/foot.php');
?>