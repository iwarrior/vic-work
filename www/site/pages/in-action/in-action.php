<?php

require_once ('./site/pages/in-action/common/head.php');



function renderInActionListItem($data) {



    ?>

    <div class="gd-in-action-item">
        <div class="l-block">
            <div class="image">
                <figure style="background-image: url('../assets/images/in-action/list/<?=$data['image']?>')"></figure>
            </div>
        </div>
        <div class="r-block">
            <div class="description">
                <h2><?=$data['title']?></h2>
                <div class="tegs-item">

                    <?php

                    foreach ($data['tags'] as $tag) {

                        ?>

                        <span class="st-color-<?=$tag['color']?>"><?=$tag['name']?></span>

                        <?php

                    }

                    ?>

                </div>
                <div class="descr">
                    <p><?=$data['description']?></p>
                </div>

                <div class="all-article"><a class="gd-button-solid" href="/in-action/<?=$data['url']?>">Read more</a></div>
            </div>
        </div>
    </div>


    <?php


}

function tag2class($name) {
    $class = str_replace(' ','-',$name);
    $class = strtolower($class);
    return $class;
}

function renderBlock($id,$data,$separator=true) {



    ?>

    <section class="ia-list ia-<?=$id?>">

        <block onClick="document.location.href='/in-action/<?=$data['url']?>'">

            <div class="left">
                <img src="/site/assets/img/in-action/list2/<?=$data['picture']?>" alt="<?=$data['title']?>" />
            </div>

            <div class="right">
                <h2><?=$data['title']?></h2>
                <div class="tags">
                    <?php
                    foreach ($data['tags'] as $tag) {
                        ?>
                        <div class="<?=tag2class($tag)?>"><?=$tag?></div>
                        <?php
                    }
                    ?>
                </div>
                <p><?=$data['description']?></p>
                <a class="read-more" href="/in-action/<?=$data['url']?>">Read more</a>
            </div>

        </block>

    </section>




    <?php

    if ($data['bottomSeparator']) {
    }


}

function renderBlockWithBg($id,$data) {


    $bgUrl = "/site/assets/img/in-action/list2/".$data['bgImg'];
    $pictureUrl = "/site/assets/img/in-action/list2/".$data['picture'];

    ?>
    <section class="ia-list with-bg ia-<?=$id?>" style="background-image: url('<?=$bgUrl?>'); background-position: left bottom;">
        <block onClick="document.location.href='/in-action/<?=$data['url']?>'">
            <div class="left"><img src="<?=$pictureUrl?>"></div>
            <div class="right">
                <h2><?=$data['title']?></h2>
                <div class="tags">
                    <?php
                    foreach ($data['tags'] as $tag) {
                        ?>
                        <div class="<?=tag2class($tag)?>"><?=$tag?></div>
                        <?php
                    }
                    ?>
                </div>
                <p><?=$data['description']?></p>
                <a class="read-more" href="/in-action/<?=$data['url']?>">Read more</a>
            </div>
        </block>

    </section>



    <?php


}





pageHeader("IN ACTION","Inspiring recipes for solving work organization challenges");


?>



<!--    <ul class="ia-list">-->
        <?php
        foreach ($inActionList as $id=>$item) {

            switch ($item['type']) {
                case 'bg':
                    renderBlockWithBg($id,$item);
                    break;
                default:
                    renderBlock($id,$item);
                    break;
            }


        }
        ?>
<!--    </ul>-->






<?php
require_once ('./site/pages/common/footer.php');
?>