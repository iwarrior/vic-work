var config = {
    apiUrl: '/api/auth/'
    // apiUrl: "//api.goodday.loc:8083/"
    // apiUrl: "https://www.goodday.work/api/auth/"
};

config.api = {
    login: config.apiUrl + "login",
    registration: config.apiUrl + "registration",
    activation: config.apiUrl + "registration/activation",
    resendEmail: config.apiUrl + "registration/resend",
    invitationDetails: config.apiUrl + "registration/invitation",


    invitation: config.apiUrl + "invitation",

    passwordRecovery: config.apiUrl + "password-recovery",
    passwordReset: config.apiUrl + "password-reset",

    support: config.apiUrl + "support",

    // marketingEmail: config.apiUrl + 'm-email/',
    marketingUnsubscribe: config.apiUrl + 'unsubscribe/',

    confirmEmail:  config.apiUrl + 'email/confirmation'

};



function googleLogin_onSuccess(googleUser) {
    var id_token = googleUser.getAuthResponse().id_token;
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/api/integration/google/token-signin');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
//                console.log('response: ' + xhr.responseText);
        document.location.href='/'
    };
    xhr.send('idToken=' + id_token);
    xhr.send('tSrc=' + getCookie('tsrc'));
    xhr.send('tRef=' + getCookie('tref'));
}
function googleLogin_onFailure(error) {
    console.error(error);
}
function googleLogin_renderButton() {
    gapi.signin2.render('my-signin2', {
        'scope': 'profile email',
        'width': 175,
        'height': 38,
        'longtitle': true,
        'theme': 'light', //dark
        'onsuccess': googleLogin_onSuccess,
        'onfailure': googleLogin_onFailure
    });
}


function getCookie(name) {
    var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
    return v ? v[2] : null;
}


function sticky_relocate() {
    var window_top = $(window).scrollTop() + 12; // the "12" should equal the margin-top value for nav.stick
    var div_top = $('#nav-anchor').offset().top;
    if (window_top > div_top) {
        $('nav').addClass('stick');
    } else {
        $('nav').removeClass('stick');
    }
}

var product = {


    goto: function(section) {
        $('html, body').animate({
            scrollTop: $("#"+section).offset().top
        }, 1000);
    }

};

var invitation = {

    validate: function(invitationKey) {


        var request = $.ajax({
            url: config.api.invitation,
            method: "GET",
            data: {
                key: invitationKey,
                zz: moment().format("ZZ")
            },
            dataType: "json",
            xhrFields: {
                withCredentials: true
            }
        });

        request.always(function() {
            $(".box-loading").addClass('hidden');
        });

        request.done(function( reply ) {

            if (reply.action == 'join-company') {
                invitation.showJoinScreen(invitationKey,reply);
            } else {

                invitation.showSingUpScreen(invitationKey,reply);
            }

        });

        request.fail(function( jqXHR, textStatus ) {

            var response = jqXHR.responseJSON;

            var errors = {};
            if (response && response.message) {
                errors = response.message;
            }

            $(".box-error").removeClass('hidden');

        });



    },

    showJoinConfirmationScreen: function(companyName) {

        $(".box-join-confirmation .company-name").html(companyName);

        $(".box-join").addClass('hidden');
        $(".box-ivitation-signup").addClass('hidden');

        $(".box-join-confirmation").removeClass('hidden');
    },

    showJoinScreen: function(invitationKey,data) {

        $(".box-join .to-user-name").html(data.toUserName);
        $(".box-join .from-user-name").html(data.fromUserName);
        $(".box-join .company-name").html(data.companyName);
        $(".box-join .h-company-name").html(data.companyName);
        $(".box-join .block-email .email").html(data.email);

        $(".box-join").removeClass('hidden');

        var requestSent = false;

        $(".box-join form").on('submit',function() {

            if (!requestSent) {

                var request = $.ajax({
                    url: config.api.invitation + '/accept',
                    method: "POST",
                    data: {
                        'key': invitationKey
                    },
                    dataType: "json",
                    xhrFields: {
                        withCredentials: true
                    }
                });

                request.done(function(data) {
                    invitation.showJoinConfirmationScreen(data.companyName);
                });

                request.fail(function(error) {
                    var errorMessage='';
                    switch (error.status) {
                        default:
                            formHelper.showErrors("Unknown server error");
                            break;
                    }
                });

                requestSent = true;
            }



            return false;
        });


    },

    showSingUpScreen: function(invitationKey, data) {

        $(".box-form .to-user-name").html(data.name);
        $(".box-form .from-user-name").html(data.fromUserName);
        $(".box-form .company-name").html(data.companyName);

        $(".box-form .h-company-name").html(data.companyName);
        $(".box-form .block-email .email").html(data.email);

        if (!data.name) $(".p-name").hide();

        $("form input[name=name]").val(data.name);

        var select = $('form select[name=timezone]');
        if(select.prop) {
            var options = select.prop('options');
        }
        else {
            var options = select.attr('options');
        }
        $('option', select).remove();

        $.each(data.timezones, function(val, text) {
            console.log(val, text);
            options[options.length] = new Option(text, text);
        });
        select.val(data.timezone);

        $(".box-form").removeClass('hidden');


        $("form").on('submit',function() {

            var data = {
                name: $('input[name=name]').val(),
                password: $('input[name=password]').val(),
                timezone: $('select[name=timezone]').val(),
                key: invitationKey,
                tsrc: 'invitation',
                tref: '',
                zz: moment().format("ZZ")
            };

            var request = $.ajax({
                url: config.api.invitation + '/signup',
                method: "POST",
                data: data,
                dataType: "json",
                xhrFields: {
                    withCredentials: true
                }
            });

            request.done(function(data) {
                invitation.showJoinConfirmationScreen(data.companyName);
            });

            request.fail(function(error) {
                var errorMessage='';

                switch (error.status) {
                    case 400:
                        $.each(error.responseJSON.message,function(msg,field){
                            errorMessage = errorMessage + field +"</br>";
                        });
                        formHelper.showErrors(errorMessage);
                        break;
                    case 409:

                        formHelper.showErrors("Email "+email+" is already in use. Use password recovery or different email to create a new account");
                        break;
                    default:
                        formHelper.showErrors("Unknown server error");
                        break;
                }

                $(".p-name").hide();
                $(".p-message").hide();
            });

            return false;
        });
    }

};

var support = {


    init: function () {

        $("#sup-link-support").on('click',function(){ support.changeSection('support') });
        $("#sup-link-sales").on("click",function(){support.changeSection('sales')});
        $("#sup-link-general").on('click',function(){support.changeSection('general')});

    },

    changeSection: function(name) {

        $("#sup-link-support").removeClass("active");
        $("#sup-link-sales").removeClass("active");
        $("#sup-link-general").removeClass("active");

        $("#sup-link-"+name).addClass("active");



        $("#sup-support").removeClass("active");
        $("#sup-sales").removeClass("active");
        $("#sup-general").removeClass("active");

        $("#sup-"+name).addClass("active");

    },

    resetForm: function() {
        $('input[name=name]').val('');
        $('input[name=email]').val('');
        $('input[name=phone]').val('');
        $('textarea[name=message]').val('');
    },

    initForm: function() {


        $('textarea[name=message]').on("click",function(){
            if ( $('textarea[name=message]').val()=='Message') {
                $('textarea[name=message]').val('');
            }
        });

        $("form").on('submit',function(){

            var errorMessage = '';

            var data = {
                name: $('input[name=name]').val(),
                email: $('input[name=email]').val(),
                phone: $('input[name=phone]').val(),
                message: $('textarea[name=message]').val()
            };

            var request = $.ajax({
                url: config.api.support,
                method: "POST",
                data: data,
                dataType: "json",
                xhrFields: {
                    withCredentials: true
                }
            });

            request.done(function( reply ) {
                formHelper.clearErrors();
                support.resetForm();
                $('.confirmation').removeClass('hidden');

            });

            request.fail(function(error) {

                $('.confirmation').addClass('hidden');

                switch (error.status) {
                    case 400:
                        $.each(error.responseJSON.message,function(msg,field){
                            errorMessage = errorMessage + field +"</br>";
                        });
                        formHelper.showErrors(errorMessage);
                        break;
                    default:
                        formHelper.showErrors("Unknown error");
                        break;
                }
            });

            return false;
        });

    }

};

var formHelper = {


    showErrors: function(errorMessages) {

        var errors = [];
        var errorsStr = "";

        if (typeof errorMessages == 'string') {
            errors.push(errorMessages);
        } else {

        }

        $.each(errors,function(key,errMsg){
            errorsStr+="<div>"+errMsg+"</div>";
        });

        $(".form-errors").removeClass('hidden');
        $(".form-errors .block").removeClass('hidden');
        $(".form-errors .errors").html(errorsStr);

    },

    hideErrors: function(errorMessage) {
        $(".form-errors-row").addClass('hidden');
        $(".form-errors").html('').hide();
    },

    clearErrors: function() {
        $(".form-errors .block").addClass('hidden');
        $(".form-errors .errors").html('');
    }

};

var passwordRecovery = {


    initPageRequest: function () {

        $("form").on('submit',function(){

            var errorMessage;
            var email = $('input[name=email]').val();
            var data = {
                email: email
            };

            var request = $.ajax({
                url: config.api.passwordRecovery,
                method: "POST",
                data: data,
                dataType: "json",
                xhrFields: {
                    withCredentials: true
                }
            });

            request.done(function( reply ) {
                document.location.href="/password-recovery/sent";
            });

            request.fail(function(error) {
                switch (error.status) {
                    case 400:
                        $.each(error.responseJSON.message,function(msg,field){
                            errorMessage = errorMessage + field +"</br>";
                        });
                        formHelper.showErrors(errorMessage);
                        break;
                    case 409:

                        formHelper.showErrors("No account with email "+email+" found");
                        break;
                    default:
                        formHelper.showErrors("Unknown error");
                        break;
                }
            });






            return false;
        });

    },

    checkResetCode: function(code) {

        var request = $.ajax({
            url: config.api.passwordReset,
            method: "GET",
            data: {
                code: code
            },
            dataType: "json",
            xhrFields: {
                withCredentials: true
            }
        });
        request.done(function( reply ) {
            $(".box-reset-form").removeClass('hidden');
            passwordRecovery.initResetForm(code);
        });
        request.fail(function(error) {
            $(".box-reset-error").removeClass('hidden');
        });

    },

    initResetForm: function(code) {


        $("form").on('submit',function() {

            var password = $('input[name=password]').val();
            var passwordConfirmation = $('input[name=passwordConfirmation]').val();

            var data = {
                password: password,
                passwordConfirmation: passwordConfirmation,
                code: code
            };

            var request = $.ajax({
                url: config.api.passwordReset,
                method: "PUT",
                data: data,
                dataType: "json",
                xhrFields: {
                    withCredentials: true
                }
            });

            request.done(function( reply ) {
                document.location.href = "/password/reset-confirmation";
            });

            request.fail(function(error) {

                switch (error.status) {
                    case 400:
                        var errorMessage = '';
                        $.each(error.responseJSON.message,function(msg,field){
                            errorMessage = errorMessage + field +"</br>";
                        });
                        formHelper.showErrors(errorMessage);
                        break;
                    case 409:
                        formHelper.showErrors("No account with email "+email+" found");
                        break;
                    default:
                        formHelper.showErrors("Unknown error");
                        break;
                }
            });

            return false;
        });
    }

};

var login = {

    initForm: function () {

        $("form").on('submit',function(){

            formHelper.clearErrors();

            var errorMessage;
            var data = {
                email: $('input[name=email]').val(),
                password: $('input[name=password]').val(),
                rememberMe: ($('input[name=remember]').prop('checked')==true)
            };

            var request = $.ajax({
                url: config.api.login,
                method: "POST",
                data: data,
                dataType: "json",
                xhrFields: {
                    withCredentials: true
                }
            });

            request.done(function( reply ) {
                document.location.href = "/";
            });

            request.fail(function( error ) {
                var errorMsgs = (error.responseJSON.message)?error.responseJSON.message:"Unknown error";
                formHelper.showErrors(errorMsgs);
            });

            return false;
        });

    }

};

var registration = {

    isBusy: false,


    activate: function(activationKey) {



        var request = $.ajax({
            url: config.api.activation,
            method: "POST",
            data: {
                activationKey: activationKey,
                zz: moment().format("ZZ")
            },
            dataType: "json",
            xhrFields: {
                withCredentials: true
            }
        });

        request.done(function( reply ) {
            //$(".confirmation").removeClass("hidden");
            document.location.href = '/join/welcome'
        });

        request.fail(function( jqXHR, textStatus ) {
            $(".activation-error").removeClass("hidden");
        });



    },


    initForm: function(tSrc, tRef) {

        $("form").on('submit',function() {

            var email = $('input[name=email]').val();

            var data = {
                name: $('input[name=name]').val(),
                email: email,
                password: $('input[name=password]').val(),
                tRef: tRef,
                tSrc: tSrc,
                zz: moment().format("ZZ")
            };

            if (!this.isBusy) {

                this.isBusy = true;

                var request = $.ajax({
                    url: config.api.registration,
                    method: "POST",
                    data: data,
                    dataType: "json",
                    xhrFields: {
                        withCredentials: true
                    }
                });

                request.done(function( reply ) {
                    this.isBusy = false;
                    document.location.href = "/join/email-confirmation?e="+email;
                }.bind(this));

                request.fail(function(error) {
                    var errorMessage='';
                    this.isBusy = false;

                    switch (error.status) {
                        case 400:
                            $.each(error.responseJSON.message,function(msg,field){
                                errorMessage = errorMessage + field +"</br>";
                            });
                            formHelper.showErrors(errorMessage);
                            break;
                        case 409:
                            formHelper.showErrors("Email "+email+" is already in use. Use password recovery or different email to create a new account");
                            break;
                        default:
                            formHelper.showErrors("Unknown server error");
                            break;
                    }
                }.bind(this));
            }



            return false;
        });
    }


};

var emailConfirmation = {

  activate: function(activationKey) {

    var request = $.ajax({
      url: config.api.confirmEmail,
      method: "POST",
      data: {
        confirmCode: activationKey,
        zz: moment().format("ZZ")
      },
      dataType: "json",
      xhrFields: {
        withCredentials: true
      }
    });

    request.done(function( reply ) {
      document.location.href = '/email-confirmation/welcome'
    });

    request.fail(function( jqXHR, textStatus ) {
      $(".activation-error").removeClass("hidden");
    });



  }
};

var start = {

    initForm: function(formName) {

        $("form[name="+formName+"]").on('submit',function(){
            var email =  $("form[name="+formName+"] input[name=email]").val();
            document.location.href = "/join?e="+email;
            return false;
        });

        $("form[name="+formName+"] .submit").on('click',function(){
            $("form[name="+formName+"]").submit();
            return false;
        });
  },

    initFooterForm: function() {

        var $form = $("form[name=footer-quick-start]");

        $form.on('submit',function(){

            var email =  $form.find('input').val();
            document.location.href = "/join?e="+email;
            return false;
        });

        return false;
    }


};

var mobileMenu = {

    init: function() {

        $('nav mobile .menu').on("click",function(){


            $("#menu-mobile").removeClass('hidden');

        });

        $('#menu-mobile .close').on("click",function(){
            $("#menu-mobile").addClass('hidden');
        });

    }


};

var unsubscribe = {

    checkEID: function(eid) {

        var request = $.ajax({
            url: config.api.marketingUnsubscribe + eid,
            method: "GET",
            data: {},
            dataType: "json",
            xhrFields: {
                withCredentials: true
            }
        });

        request.done(function( reply ) {

            $("#unsubscribe-message").html(reply.message);
            $(".email-address").html(reply.email);
            $("#unsubscribe-confirm").removeClass('hidden');

            unsubscribe.initForm(eid);

        });

        request.fail(function( jqXHR, textStatus ) {

            $("#unsubscribe-error").removeClass('hidden');

        });

    },

    initForm: function(eid) {

        $('#btn-unsubscribe').click(function(){

            console.log('unsubscribe',eid);

            var request = $.ajax({
                url: config.api.marketingUnsubscribe + eid,
                method: "POST",
                data: {},
                dataType: "json",
                xhrFields: {
                    withCredentials: true
                }
            });

            request.done(function( reply ) {
                $("#unsubscribe-confirm").addClass('hidden');
                $("#unsubscribe-confirmation").removeClass('hidden');
            });

            request.fail(function( jqXHR, textStatus ) {
                $("#unsubscribe-confirm").addClass('hidden');
                $("#unsubscribe-error").removeClass('hidden');
            });



        })

    }

};

$(function(){

    mobileMenu.init();

    start.initFooterForm();


});