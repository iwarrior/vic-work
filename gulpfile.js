var gulp		 	= require('gulp'),
	sass 				= require('gulp-sass'),
	browserSync = require('browser-sync'),
	// bourbon 		= require('node-bourbon'),

	concat			=	require('gulp-concat'),
	uglify			= require('gulp-uglifyjs'),
	cssnano			= require('gulp-cssnano'),
	rename			= require('gulp-rename'),
	del					= require('del'),
	notify 				= require('gulp-notify'),
	sourcemaps 		= require('gulp-sourcemaps'),
	// boot = require('jquery'),
	// boot = require('bootstrap'),
  path 						= require('path');
	pngquant		= require('imagemin-pngquant'),
	cache				=	require('gulp-cache');
	autoprefixer= require('gulp-autoprefixer');

// SVG спрайты
var svgstore 			= require('gulp-svgstore');
var svgmin 				= require('gulp-svgmin');
var cheerio 			= require('gulp-cheerio');

/*************** все для картинок ***************/
var buffer 				= require('vinyl-buffer');							// какой-то буфер (не совсем разобрался как это работает)
var imagemin			= require('gulp-imagemin');							// минификация картинок
var spritesmith		= require('gulp.spritesmith');					// создание спрайтов

//добавление переменной к ссылке
var hash_src = require("gulp-hash-src");

//добавление блоков html
var fileinclude = require('gulp-file-include')

gulp.task('fileinclude', function() {
  gulp.src(['app/**/*.html'])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('dest'));
});



gulp.task('error', function(){
	return gulp.src('scss/**/*.scss')
	.pipe(notify('error!'))
});



/****************************************
 
****************************************/

gulp.task('sass', function() {
	// return gulp.src('src/**/*.+(scss|sass|css)')
	return gulp.src('src/main.scss')
	.pipe(sourcemaps.init())
	.pipe(sass({
		includePaths: require('node-bourbon').includePaths
	}).on('error', sass.logError))
	.pipe(sourcemaps.write())
	.pipe(autoprefixer(['last 15 versions', '>1%', 'ie 8'], {cascade:true}))
	// .pipe(cssnano())
	.pipe(gulp.dest('www/site/assets/css/'))
	.pipe(notify('Done!'))
	.pipe(browserSync.stream());
});


gulp.task('css', function() {
	return gulp.src('app/css/style.css')
	.pipe(browserSync.stream());
});

gulp.task('scripts', function(){
	return gulp.src([
		'app/js/*.js',
		'app/libs/jquery/dist/jquery.min.js',
		'app/libs/magnific-popup/dist/jquery.magnific-popup.min.js',
		])
	.pipe(concat('libs.min.js'))
	// .pipe(uglify())
	.pipe(gulp.dest('dist/js'))
	.pipe(browserSync.stream());
});

gulp.task('css-libs', ['sass'], function(){
	return gulp.src('app/css/libs.css')
	// .pipe(cssnano())
	// .pipe(rename({suffix: '.min'}))
	.pipe(gulp.dest('app/css'));
})

gulp.task('browser-sync', function(){
	browserSync({
		proxy: 'goodday.loc',
		notify: false
	});
});

gulp.task('clean', function(){
	return del.sync('dist')
});

gulp.task('clear', function(){
	return cache.clearAll();
});

gulp.task('watch', ['browser-sync','sass', 'php'],function(){
	gulp.watch('src/**/*.+(scss|sass|css)',['sass']);
	// gulp.watch('app/**/*.html', ['html']);
	// gulp.watch('app/js/**/*.js', ['scripts']);
	// gulp.watch('app/js/**/*.js', ['scripts']);
	// gulp.watch('www/**/*.php',);
	gulp.watch('www/**/*.php', browserSync.reload);
});

gulp.task('build', ['clean', 'img', 'sass', 'scripts', 'svgstore'], function(){

	var buildCss = gulp.src([
		'app/css/main.css',
		'app/css/libs.min.css'
		])
		.pipe(gulp.dest('dist/css'));

	var buildFonts = gulp.src('app/fonts/**/*')
		.pipe(gulp.dest('dist/fonts'));

	var	buildJs = gulp.src('app/js/**/*')
		.pipe(gulp.dest('dist/js'));

	var buildHtml = gulp.src('app/**/*.html')
		.pipe(gulp.dest('dist'));

})

/****************************************
Создание спрайтов 
****************************************/

gulp.task('sprite', function () {
  var spriteData = gulp.src('app/sprite_icons/*.*')			// путь, откуда берем картинки для спрайта
  	.pipe(spritesmith({
	    imgName: 'sprite.png',
	    cssName: '_sprite.sass',
	    padding: 5
	  }));

	spriteData.img.pipe(gulp.dest('app/img/'));					// путь, куда сохраняем картинку
	spriteData.css.pipe(gulp.dest('app/sass/'));					// путь, куда сохраняем стили
});

/****************************************
	* Созадние svg спрайта 
	****************************************/
gulp.task('svgstore', function () {
    return gulp
        .src('app/svg/*.svg')

        .pipe(svgmin())
        .pipe(cheerio({
            run: function ($) {
                $('defs').html('');
            },
            parserOptions: { xmlMode: true }
        }))
        .pipe(svgstore({ inlineSvg: true }))
        .pipe(rename({basename: 'sprite'}))
        .pipe(gulp.dest('dist/img'));
});





/****************************************
html 
****************************************/

gulp.task('html',function(){
	return gulp.src('app/**/*.html')
	.pipe(hash_src({build_dir: "dist", src_path: "app"}))
	.pipe(fileinclude({
      prefix: '@@',
      // basepath: '@file'
    }))
	.pipe(gulp.dest('dist/'))
	.pipe(browserSync.stream());
});

/****************************************
оптимизация картинок
****************************************/
gulp.task('img', function(){
	return gulp.src('app/img/**/*')
	// .pipe(cache(imagemin({
	// 	interlaced: true,
	// 	progtessive: true,
	// 	svgPlugins: [{removeViewBox:false}],
	// 	une: [pngquant()]
	// })))
	.pipe(gulp.dest('dist/img'));
});


/****************************************
	* сомтрим за php 
	****************************************/

gulp.task('php',function(){
	// return gulp.
	// return gulp.src('app/**/*.php')
	// .pipe(browserSync.stream())
	// .pipe(gulp.dest('dist/'))
});

/****************************************
строим проект
****************************************/
gulp.task('build', ['clean', 'sprite', 'sass', 'html', 'scripts', 'img', 'svgstore'], function(){
	var buildFonts = gulp.src('app/fonts/**/*')
		.pipe(gulp.dest('dist/fonts'));

	var buildFancybox = gulp.src('app/fancybox/**/*')
		.pipe(gulp.dest('dist/fancybox'));

	var buildForm = gulp.src('app/res.php')
		.pipe(gulp.dest('dist'));
});
